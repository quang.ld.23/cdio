﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cdio447.Models;

namespace Cdio447.ViewModels
{
    public class GameSupport: IGamesSupport
    {
        private DBContext ctx;

        public GameSupport()
        {
            ctx = new DBContext();
        }
        public List<games> ListAllGames(string searchString) {
            return ctx.games.Where(x => x.name.StartsWith(searchString) || searchString == null).OrderBy(x => x.name).ToList();
        }

        public bool CheckName(games s) {
            return ctx.games.Any(u => u.name.ToUpper() == s.name.ToUpper());
        }

        public void InsertGame(games g) {
            var gameData = new games()
            {
                name = g.name,
                link = g.link,
                description = g.description
            };
            ctx.games.Add(gameData);
            ctx.SaveChanges();

        }

        public games GamesByID(int? id) {
            return ctx.games.Where(u => u.id == id).SingleOrDefault();
            
        }

        public void UpdateGame(games g) {
            games gameData = GamesByID(g.id);
            gameData.name = g.name;
            gameData.link = g.link;
            gameData.description = g.description;
            ctx.SaveChanges();
        }

        public void DeleteGame(int gameid) {
            games gameData = GamesByID(gameid);
            ctx.games.Remove(gameData);
            ctx.SaveChanges();
        }
    }
}