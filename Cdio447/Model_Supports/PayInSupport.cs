﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Cdio447.Model_Supports
{
    public class PayInSupport
    {
        [Required(ErrorMessage = "Số tiền cần nạp không được bỏ trống")]
        [RegularExpression(@"^[1-9][0-9]+$", ErrorMessage = "Số tiền nạp không đúng định dạng")]
        public string amount { get; set; }
        [Required(ErrorMessage = "Vui lòng chọn ngân hàng để nạp tiền")]
        public string banks { get; set; }
        
        public int bank_id { get; set; }
    }
}