﻿using Cdio447.Models;
using System.Collections.Generic;

namespace Cdio447.ViewModels
{
    public interface IGamesSupport
    {
        List<games> ListAllGames(string searchString);

        bool CheckName(games s);
        
        void InsertGame(games g);

        games GamesByID(int? id);

        void UpdateGame(games g);

        void DeleteGame(int gameid);
    }
}