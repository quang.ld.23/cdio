﻿using Cdio447.Models;
using Cdio447.ViewModels;
using System.Collections.Generic;
using Cdio447.Areas.Admin.Models;
using System.Linq;

namespace Cdio447.Model_Supports
{
    public interface IManUsers
    {
        List<InfoPlayerViewModel> Listallusers();

        void insertUser(userView user, string avatar);

        InfoPlayerViewModel GetuserbyID(int id);

        void UpdateUser(users user);

        List<cities> listciy();

        bool checkUser(userView u);

        bool checkPhone(userView u);

        bool checkEmail(userView u);

        bool checkDisplayName(userView u);

        short? changeStatus(int id);
    }
}