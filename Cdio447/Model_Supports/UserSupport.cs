﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cdio447.Models;
using Cdio447.ViewModels;
namespace Cdio447.ViewModels
{
    
    public class UserSupport
    {
        
        DBContext db = new DBContext();
        //list all player for hompage
        public List<InfoPlayerViewModel> ListUserHP()
        {
            var models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id && a.status == 1 //online
                          && a.status != null
                          && a.status != 2
                          && a.status != 3
                          select new
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              current_money = a.current_money,
                              status = a.status,
                              title = b.title,
                              description = b.description,
                              link_fb = b.link_fb,
                              link_insta = b.link_insta,
                              link_messenger = b.link_messenger,
                              link_youtube = b.link_youtube,
                              total_time = b.total_time,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag
                        }
            ).AsEnumerable().Select(x => new InfoPlayerViewModel() {
                user_id = x.user_id,
                display_name = x.display_name,
                avatar = x.avatar,
                current_money = x.current_money,
                status = x.status,
                title = x.title,
                description = x.description,
                link_fb = x.link_fb,
                link_insta = x.link_insta,
                link_messenger = x.link_messenger,
                link_youtube = x.link_youtube,
                total_time = x.total_time,
                amount_in_one = x.amount_in_one,
                tag = x.tag
            });
            return models.ToList();
        }
        //View Detail Client with id
        public InfoPlayerViewModel ViewDetail(int id)
        {
            var models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id
                          where b.user_id == id
                          && a.status != null
                          select new InfoPlayerViewModel()
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              current_money = a.current_money,
                              status = a.status,
                              title = b.title,
                              description = b.description,
                              link_fb = b.link_fb,
                              link_insta = b.link_insta,
                              link_messenger = b.link_messenger,
                              link_youtube = b.link_youtube,
                              total_time = b.total_time,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag,
                          }).SingleOrDefault();
            return (InfoPlayerViewModel)models;
        }
        public int[] getPlayerGame(int id)
        {
            var selectArrGame = db.player_game.Where(x => x.user_id.Equals(id));
            var count = selectArrGame.ToList().Count;
            List<int> arr_game_id = new List<int>();
            if (selectArrGame != null || count <= 0)
            {
                foreach (var slg in selectArrGame)
                    arr_game_id.Add(slg.game_id);
            }
            return arr_game_id.ToArray();
        }
    }
}