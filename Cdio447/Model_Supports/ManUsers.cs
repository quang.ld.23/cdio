﻿using Cdio447.Areas.Admin.Models;
using Cdio447.Models;
using Cdio447.Supports;
using Cdio447.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cdio447.Model_Supports
{
    public class ManUsers : IManUsers
    {
        private DBContext ctx;

        public ManUsers()
        {
            ctx = new DBContext();
        }

        public List<InfoPlayerViewModel> Listallusers()
        {
            var list = (
                        from a in ctx.users
                        join b in ctx.cities
                        on a.city_id equals b.id
                        join c in ctx.info_players
                        on a.id equals c.user_id into temp
                        from subtemp in temp.DefaultIfEmpty()
                        select new
                        {
                            user_id = a.id,
                            username = a.user_name,
                            displayname = a.display_name,
                            useremail = a.email,
                            phonenumber = a.phone,
                            current_money = a.current_money,
                            gender = a.gender,
                            tag_game = subtemp == null ? " " : subtemp.tag,
                            role = a.role,
                            city = b.name,
                            stt = a.status,
                            total_time_rent = a.total_time_rent,
                            total_time = subtemp == null ? 0 : subtemp.total_time,
                            amount_in_one = subtemp == null ? 0 : subtemp.amount_in_one
                        }
              ).Select(u => new InfoPlayerViewModel()
              {
                  user_id = u.user_id,
                  display_name = u.displayname,
                  user_name = u.username,
                  email = u.useremail,
                  phone = u.phonenumber,
                  gender = u.gender,
                  tag = u.tag_game,
                  role = u.role,
                  namecity = u.city,
                  status = u.stt,
                  total_time_rent = u.total_time_rent,
                  total_time = u.total_time,
                  amount_in_one = u.amount_in_one
              });
            return list.ToList();
        }

        public InfoPlayerViewModel GetuserbyID(int id)
        {
            IQueryable<InfoPlayerViewModel> listbyId;
            listbyId = (
                        from a in ctx.users
                        join b in ctx.cities
                        on a.city_id equals b.id
                        join c in ctx.info_players
                        on a.id equals c.user_id into temp
                        from subtemp in temp.DefaultIfEmpty()
                        where a.id == id
                        select new
                        {
                            user_id = a.id,
                            username = a.user_name,
                            displayname = a.display_name,
                            useremail = a.email,
                            phonenumber = a.phone,
                            current_money = a.current_money,
                            gender = a.gender,
                            //tag_game = subtemp == null ? " " : subtemp.tag,
                            //role = a.role,
                            city = b.name,
                            //stt = a.status,
                            //total_time_rent = a.total_time_rent,
                            //total_time = subtemp == null ? 0 : subtemp.total_time,
                            //amount_in_one = subtemp == null ? 0 : subtemp.amount_in_one
                        }
              ).Select(u => new InfoPlayerViewModel()
              {
                  user_id = u.user_id,
                  display_name = u.displayname,
                  user_name = u.username,
                  email = u.useremail,
                  phone = u.phonenumber,
                  gender = u.gender,
                  //tag = u.tag_game,
                  //role = u.role,
                  namecity = u.city,
                  current_money = u.current_money
                  //status = u.stt,
                  //total_time_rent = u.total_time_rent,
                  //total_time = u.total_time,
                  // amount_in_one = u.amount_in_one
              });
            return listbyId.FirstOrDefault();
        }

        public void insertUser(userView user, string avatar)
        {
            var userData = new users
            {
                user_name = user.user_name,
                password = Encryptor_MD5.MD5Hash(user.password),
                display_name = user.display_name,
                phone = user.phone,
                email = user.email,
                role = 1,
                gender = user.gender,
                city_id = user.city_id,
                avatar = avatar
            };
            ctx.users.Add(userData);
            ctx.SaveChanges();
        }

        public void UpdateUser(users user)
        {
            throw new NotImplementedException();
        }

        public List<cities> listciy()
        {
            return ctx.cities.ToList();
        }

        public bool checkUser(userView u)
        {
            return ctx.users.Any(s => s.user_name.Equals(u.user_name, StringComparison.CurrentCultureIgnoreCase));
        }

        public bool checkPhone(userView u)
        {
            return ctx.users.Any(s => s.phone == u.phone);
        }

        public bool checkEmail(userView u)
        {
            return ctx.users.Any(s => s.email == u.email);
        }

        public bool checkDisplayName(userView u)
        {
            return ctx.users.Any(s => s.display_name.Equals(u.display_name, StringComparison.CurrentCultureIgnoreCase));
        }

        public short? changeStatus(int id)
        {
            var user = ctx.users.Find(id);
            if (user.status != 3)
            {
                user.status = 3;
            }
            else
            {
                user.status = null;
            }
            ctx.SaveChanges();
            return user.status;
        }
    }
}