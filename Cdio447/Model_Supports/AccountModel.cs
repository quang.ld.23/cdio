﻿using Cdio447.Models;
using Cdio447.Supports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Cdio447.Code;
namespace Cdio447.ViewModels
{
    internal class AccountModel
    {
        private DBContext context = null;

        public AccountModel()
        {
            context = new DBContext();
        }

        public long Insert(users entity)
        {
            context.users.Add(entity);
            context.SaveChanges();
            return entity.id;
        }

        public users GetById(string userName)
        {
            return context.users.SingleOrDefault(x => x.user_name == userName);
        }
        public bool check_pass(string name , string pass)
        {
            var user = new users();
            user = context.users.SingleOrDefault(x => x.user_name == name);
            if (Encryptor_MD5.MD5Hash(pass) == user.password)
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }
        public users GetBymail(string mail)
        {
            return context.users.SingleOrDefault(x => x.email == mail);
        }

        public bool updatpass(string pass, int id)
        {

            try
            {
                var user = new users();
                user = context.users.Find(id);
                user.password = Encryptor_MD5.MD5Hash(pass);
                context.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }



        public List<users> AvatarUser(string username)
        {
            return context.users.Where(x => x.user_name == username).ToList();
        }

     
        public List<cities> listciy()
        {
            return context.cities.ToList();
        }

        public bool Check_User_name(string username)
        {
            return context.users.Count(x => x.user_name == username) > 0;

        }

        public bool Check_Email(string email)
        {
            return context.users.Count(x => x.email == email) > 0;

        }

        public int Login(string userName, string passWord)
        {
            if (passWord == null || userName == null)  // nhap rong
                return -1;

            {
                var result = context.users.SingleOrDefault(x => x.user_name == userName);
                if (result == null)//user_name k ton tai trong database
                {
                    return 0;
                }
                else
                {
                    if (result.password == Encryptor_MD5.MD5Hash(passWord))
                    {
                        if (result.role == 1)
                        {
                            return 1;
                        }
                        else
                        {
                            return 2;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public bool Update_infor(user_sp entity, int id)
        {
            try {

                var user = new users();
                user = context.users.Find(id);
                if(entity.avatar == ".jpg")
                {
                    user.display_name = entity.display_name;
                    user.phone = entity.phone;
                    user.gender = entity.gender;
                    user.city_id = entity.city_id;
                    
                }
                else
                {
                    user.display_name = entity.display_name;
                    user.phone = entity.phone;
                    user.gender = entity.gender;
                    user.city_id = entity.city_id;
                    user.avatar = entity.avatar;
                }
                context.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}