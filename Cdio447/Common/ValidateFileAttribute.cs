﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cdio447.Common
{
    public class ValidateFileAttribute : RequiredAttribute
    {
        public override bool IsValid(object value)
        {
            int MaxContentLength = 1024 * 1024 *2;
            string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png" };

            var file = value as HttpPostedFileBase;

            if (file == null)
                return false;
            else if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
            {
                ErrorMessage = "Chỉ được upload ảnh dạng: " + string.Join(", ", AllowedFileExtensions);
                return false;
            }
            else if (file.ContentLength > MaxContentLength)
            {
                ErrorMessage = "Dung lượng ảnh quá lớn, tối đa là : " + (MaxContentLength / 1024).ToString() + "MB";
                return false;
            }
            else
                return true;
        }
    }
}