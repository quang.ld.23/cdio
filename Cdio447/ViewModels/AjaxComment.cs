﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cdio447.ViewModels
{
    public class AjaxComment
    {
        public string getContent { get; set; }
        public string getNumberStar { get; set; }
        public int userIdRcmt { get; set; }
    }
}