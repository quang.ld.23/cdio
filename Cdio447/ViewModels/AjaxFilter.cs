﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cdio447.ViewModels
{
    public class AjaxFilter
    {
        public string gender { get; set; }
        public string[] arr_cities { get; set; }
        public string name { get; set; }
        public string tag { get; set; }
        public string max_price { get; set; }
        public string min_price { get; set; }
    }
}