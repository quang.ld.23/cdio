﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Cdio447.ViewModels
{
    public class update_pass

    {
        
        [Required(ErrorMessage = "Vui lòng nhập Mật khẩu")]
        public string old_password { set; get; }

        
        [Required(ErrorMessage = "Vui lòng nhập Mật khẩu")]
        [StringLength(100,MinimumLength =6,ErrorMessage ="Độ dài mật khẩu từ 6 đến 30 kí tự")]
        public string new_password { set; get; }

        [Compare("new_password", ErrorMessage = "Mật khẩu không khớp.")]
        public string conf_new_password { set; get; }

    }
}