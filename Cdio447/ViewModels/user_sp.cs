﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cdio447.ViewModels
{
    public class user_sp
    {

     

        //[Required(ErrorMessage = "Vui lòng nhập tên đăng nhập")]
        //[RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Tên tài khoản không hợp lệ")]
        //[StringLength(30, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu từ 6 - 30 kí tự")]
        //public string user_name { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập PassWord")]
        //[RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Mật khẩu không hợp lệ")]
        //[StringLength(100, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu từ 6 - 100 kí tự")]
        //public string password { get; set; }

        [StringLength(100, MinimumLength = 6, ErrorMessage = "Độ dài tên hiển thị từ 6 - 100 kí tự")]
        [RegularExpression(@"^[^:>#*]+|([^:>#*][^>#*]+[^:>#*])$", ErrorMessage = "Tên hiển thị không đúng định dạng")]
        [Required(ErrorMessage = "Vui lòng nhập Tên hiển thị")]
        public string display_name { get; set; }

        [Required]
        [StringLength(50)]
        [RegularExpression(@"^\d{3}\d{3}\d{4}$", ErrorMessage = "Số điện thoại phải là 10 chữ số")]
        public string phone { get; set; }

        public short? gender { get; set; }

        public int city_id { get; set; }

        [StringLength(100)]
        public string avatar { get; set; }


    }
}