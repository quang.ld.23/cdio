﻿using System.ComponentModel.DataAnnotations;

namespace Cdio447.ViewModels
{
    public class InfoPlayerViewModel
    {
        public int user_id { get; set; }
        public string user_name { get; set; }

        [StringLength(100)]
        public string display_name { get; set; }

        [StringLength(100)]
        public string avatar { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        public string phone { get; set; }

        public short role { get; set; }

        public short? gender { get; set; }

        public string namecity { get; set; }

        public string totalcurrency { get; set; }

        public decimal? current_money { get; set; }

        public short? status { get; set; }

        [Required(ErrorMessage = "Tiêu đề không được để trống")]
        [StringLength(50)]
        public string title { get; set; }
        [Required(ErrorMessage = "Mô tả không được để trống")]
        public string description { get; set; }

        [StringLength(maximumLength: 100, ErrorMessage = "Link facebook quá dài")]
        [RegularExpression(@"(?:(?:http|https):\/\/)?(?:www.)?(facebook.com|fb.com)\/(?:(?:\w)*#!\/)?(?:pages\/)?([\w\-]*)?",
        ErrorMessage = "Link facebook không đúng định dạng")]
        public string link_fb { get; set; }

        [StringLength(maximumLength: 100, ErrorMessage = "Link instagram quá dài")]
        [RegularExpression(@"(?:(?:http|https):\/\/)?(?:www.)?(instagram.com)\/(?:(?:\w)*#!\/)?(?:pages\/)?([\w\-]*)?",
        ErrorMessage = "Link instagram không đúng định dạng")]
        public string link_insta { get; set; }

        [StringLength(maximumLength: 100, ErrorMessage = "Link messenger quá dài")]
        [RegularExpression(@"(?:(?:http|https):\/\/)?(?:www.)?(messenger.com)\/(?:(?:\w)*#!\/)?(?:pages\/)?([\w\-]*)?",
        ErrorMessage = "Link messenger không đúng định dạng")]
        public string link_messenger { get; set; }

        [StringLength(maximumLength: 20, ErrorMessage = "Youtube video Id từ 5 đến 20 ký tự", MinimumLength = 5)]
        public string link_youtube { get; set; }

        public int? total_time { get; set; }

        [Required(ErrorMessage = "Chi phí thuê không được để trống")]
        [RegularExpression("^[1-9][0-9]*$", ErrorMessage = "Số tiền không đúng định dạng")]
        public decimal? amount_in_one { get; set; }
        
        public int? total_time_rent { get; set; }
        [Required(ErrorMessage = "Từ khóa không được để trống")]
        [StringLength(maximumLength: 50,ErrorMessage = "Từ khóa từ 5 đến 50 ký tự", MinimumLength = 5)]
        public string tag { get; set; }
        [Required(ErrorMessage = "Chọn ít nhất một tựa game")]
        public int[] game_id { get; set; }
    }
}