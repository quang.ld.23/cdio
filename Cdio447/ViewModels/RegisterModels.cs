﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Cdio447.ViewModels
{
    public class RegisterModels
    { [Key]
    public int id { set; get; } 

        [Display(Name ="Tên đăng nhập")]
        [Required(ErrorMessage ="Vui lòng nhập tên đăng nhập")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage ="Tên tài khoản không hợp lệ")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu từ 6 - 30 kí tự")]
        public string user_name { set; get; }   

        [Display(Name = "PassWord")]
        [Required(ErrorMessage = "Vui lòng nhập PassWord")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Mật khẩu không hợp lệ")]
        [StringLength(100,MinimumLength =6,ErrorMessage = "Độ dài mật khẩu từ 6 - 100 kí tự")]
         public string password { set; get; }

        [Display(Name = "Xác nhận PassWord")]
        [Compare("password",ErrorMessage ="Mật khẩu không khớp.")]  
        public string confpassword { set; get ; }

        [Display(Name = "Tên hiển thị")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Độ dài mật khẩu từ 6 - 20 kí tự")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Tên hiển thị không đúng định dạng")]
        [Required(ErrorMessage = "Vui lòng nhập Tên hiển thị")]
        public string display_name { set; get; }

       
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Vui lòng nhập email")]
        [RegularExpression(@"^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$", ErrorMessage = "Địa chỉ Email không hợp lệ")]
        public string email { set; get; }

        [Display(Name = "Số điện thoại")]
        [RegularExpression(@"^\d{3}\d{3}\d{4}$", ErrorMessage = "Số điện thoại phải là 10 chữ số")]
        [Required(ErrorMessage = "Vui lòng nhập Số điện thoại")]
        public string phone { set; get; }

        [Display(Name ="Giới Tính")]
        public short gender { set; get; }

        [Display(Name ="Thành Phố")]
        [Required(ErrorMessage = "Vui lòng chọn Thành phố bạn đang sống")]
        public int city_id { set; get; }





    }
}