﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Cdio447.ViewModels
{
    public class PaymentGateway
    {
        public int user_id { get; set; }

        public int bank_id { get; set; }

        public string account_name { get; set; }

        public string account_code { get; set; }
    }
}