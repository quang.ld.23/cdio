﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Cdio447.ViewModels
{
    [DataContract]
    public class RecaptchaResult
    {  
        [DataMember(Name ="success")]
        public bool Success { set; get; }

        [DataMember(Name ="error-code")]
        public string[] ErrorCodes { set; get; }

    }
}