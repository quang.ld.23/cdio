﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Cdio447.ViewModels
{
    public class WidthdrawlViewModel
    {
        [Required(ErrorMessage = "Số tiền rút không được để trống")]
        [RegularExpression("^[1-9][0-9]*$",ErrorMessage = "Số tiền rút sai định dạng")]
        public string amount { get; set; }
    }
}