﻿"use strict";
var source = './Content/assets/dev/';
var dest = './Content/assets/';


var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyJs = require('gulp-minify');
var minifyCss = require('gulp-minify-css');
var concat = require('gulp-concat');
var babel = require("gulp-babel");
var runSequence = require('run-sequence');

gulp.task('sass', function () {
    return gulp.src(source + 'sass/client.scss')
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(gulp.dest(dest + 'css'))
});

gulp.task('sass-admin', function () {
    return gulp.src(source + 'sass/admin.scss')
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(gulp.dest(dest + 'css'))
});

gulp.task('js', function () {
    return gulp.src(source + 'js/**/*.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(minifyJs())
        .pipe(gulp.dest(dest + 'js/'))
});

//developing app
gulp.task('watch', ['sass', 'sass-admin', 'js'], function () {
    gulp.watch(source + 'sass/**/*.scss', ['sass', 'sass-admin'])
    gulp.watch(source + 'js/**/*.js', ['js'])
});

//building app
gulp.task('build', function (c) {
    runSequence('sass', 'sass-admin', 'js',
        c
    )
});