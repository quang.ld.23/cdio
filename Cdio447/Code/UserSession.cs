﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cdio447.Code
{
    [Serializable]
    public class UserSession
    {
        public int User_id { set; get; }
        public string UserName { set; get; }
        public int Role { set; get; }
        public int checkmail { set; get; }
        public string email { set; get; }
        public string user_name { set; get; }
        public string phone { set; get; }
        public decimal current_money { set; get; }
        public short status { set; get; }
    }
}