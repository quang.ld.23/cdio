﻿namespace Cdio447.Code
{
    public static class CommonConstants
    {
        public static string USER_SESSION = "USER_SESSION";

        // co thể xóa
        public static string USE_ROLE = "USE_ROLE";
        public static string SESSION_CREDENTIALS = "SESSION_CREDENTIALS";
        public static string CartSession = "CartSession";
        public static string PayInSession = "PayInSession";

        public static string CurrentCulture { set; get; }
    }
}