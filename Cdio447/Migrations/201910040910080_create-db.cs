namespace Cdio447.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.banks",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50),
                        image = c.String(maxLength: 50),
                        shortNamePayin = c.String(maxLength: 10),
                        description = c.String(storeType: "ntext"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.payinto_histories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id = c.Int(nullable: false),
                        bank_id = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        oder_code = c.String(nullable: false, maxLength: 50),
                        created_at = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.users", t => t.user_id, cascadeDelete: true)
                .ForeignKey("dbo.banks", t => t.bank_id)
                .Index(t => t.user_id)
                .Index(t => t.bank_id);
            
            CreateTable(
                "dbo.users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_name = c.String(nullable: false, maxLength: 30),
                        password = c.String(nullable: false, maxLength: 100),
                        display_name = c.String(nullable: false, maxLength: 100),
                        phone = c.String(nullable: false, maxLength: 50),
                        email = c.String(nullable: false, maxLength: 50),
                        role = c.Short(nullable: false),
                        gender = c.Short(),
                        city_id = c.Int(nullable: false),
                        avatar = c.String(maxLength: 100),
                        current_money = c.Decimal(precision: 18, scale: 0),
                        status = c.Short(),
                        total_time_rent = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.cities", t => t.city_id)
                .Index(t => t.city_id);
            
            CreateTable(
                "dbo.cities",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.comments",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id_cmt = c.Int(nullable: false),
                        user_id_rcmt = c.Int(nullable: false),
                        contents = c.String(nullable: false, storeType: "ntext"),
                        number_star = c.Short(nullable: false),
                        created_at = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.users", t => t.user_id_cmt)
                .ForeignKey("dbo.users", t => t.user_id_rcmt, cascadeDelete: true)
                .Index(t => t.user_id_cmt)
                .Index(t => t.user_id_rcmt);
            
            CreateTable(
                "dbo.dou_histories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id_dou = c.Int(nullable: false),
                        user_id_rent = c.Int(nullable: false),
                        total_time = c.Int(nullable: false),
                        total_amount = c.Decimal(nullable: false, precision: 18, scale: 0),
                        created_at = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.users", t => t.user_id_dou, cascadeDelete: true)
                .ForeignKey("dbo.users", t => t.user_id_rent)
                .Index(t => t.user_id_dou)
                .Index(t => t.user_id_rent);
            
            CreateTable(
                "dbo.info_players",
                c => new
                    {
                        user_id = c.Int(nullable: false),
                        title = c.String(maxLength: 50),
                        description = c.String(storeType: "ntext"),
                        link_fb = c.String(maxLength: 100),
                        link_insta = c.String(maxLength: 100),
                        link_messenger = c.String(maxLength: 100),
                        link_youtube = c.String(maxLength: 100),
                        total_time = c.Int(),
                        amount_in_one = c.Decimal(precision: 18, scale: 0),
                        tag = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.user_id)
                .ForeignKey("dbo.users", t => t.user_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.payment_gateways",
                c => new
                    {
                        user_id = c.Int(nullable: false),
                        bank_id = c.Int(nullable: false),
                        account_name = c.String(),
                        account_code = c.String(),
                    })
                .PrimaryKey(t => t.user_id)
                .ForeignKey("dbo.users", t => t.user_id)
                .ForeignKey("dbo.banks", t => t.bank_id)
                .Index(t => t.user_id)
                .Index(t => t.bank_id);
            
            CreateTable(
                "dbo.player_game",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        game_id = c.Int(nullable: false),
                        user_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.games", t => t.game_id, cascadeDelete: true)
                .ForeignKey("dbo.users", t => t.user_id, cascadeDelete: true)
                .Index(t => t.game_id)
                .Index(t => t.user_id);
            
            CreateTable(
                "dbo.games",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 50),
                        link = c.String(maxLength: 100),
                        description = c.String(storeType: "ntext"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.rent_transaction",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id_dou = c.Int(nullable: false),
                        user_id_rent = c.Int(nullable: false),
                        total_time = c.Short(nullable: false),
                        message = c.String(storeType: "ntext"),
                        created_at = c.DateTime(nullable: false),
                        status = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.users", t => t.user_id_dou)
                .ForeignKey("dbo.users", t => t.user_id_rent)
                .Index(t => t.user_id_dou)
                .Index(t => t.user_id_rent);
            
            CreateTable(
                "dbo.withdrawl_histories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        user_id = c.Int(nullable: false),
                        total = c.Decimal(nullable: false, precision: 18, scale: 0),
                        status = c.Short(nullable: false),
                        created_at = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.users", t => t.user_id)
                .Index(t => t.user_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.payment_gateways", "bank_id", "dbo.banks");
            DropForeignKey("dbo.payinto_histories", "bank_id", "dbo.banks");
            DropForeignKey("dbo.withdrawl_histories", "user_id", "dbo.users");
            DropForeignKey("dbo.rent_transaction", "user_id_rent", "dbo.users");
            DropForeignKey("dbo.rent_transaction", "user_id_dou", "dbo.users");
            DropForeignKey("dbo.player_game", "user_id", "dbo.users");
            DropForeignKey("dbo.player_game", "game_id", "dbo.games");
            DropForeignKey("dbo.payment_gateways", "user_id", "dbo.users");
            DropForeignKey("dbo.payinto_histories", "user_id", "dbo.users");
            DropForeignKey("dbo.info_players", "user_id", "dbo.users");
            DropForeignKey("dbo.dou_histories", "user_id_rent", "dbo.users");
            DropForeignKey("dbo.dou_histories", "user_id_dou", "dbo.users");
            DropForeignKey("dbo.comments", "user_id_rcmt", "dbo.users");
            DropForeignKey("dbo.comments", "user_id_cmt", "dbo.users");
            DropForeignKey("dbo.users", "city_id", "dbo.cities");
            DropIndex("dbo.withdrawl_histories", new[] { "user_id" });
            DropIndex("dbo.rent_transaction", new[] { "user_id_rent" });
            DropIndex("dbo.rent_transaction", new[] { "user_id_dou" });
            DropIndex("dbo.player_game", new[] { "user_id" });
            DropIndex("dbo.player_game", new[] { "game_id" });
            DropIndex("dbo.payment_gateways", new[] { "bank_id" });
            DropIndex("dbo.payment_gateways", new[] { "user_id" });
            DropIndex("dbo.info_players", new[] { "user_id" });
            DropIndex("dbo.dou_histories", new[] { "user_id_rent" });
            DropIndex("dbo.dou_histories", new[] { "user_id_dou" });
            DropIndex("dbo.comments", new[] { "user_id_rcmt" });
            DropIndex("dbo.comments", new[] { "user_id_cmt" });
            DropIndex("dbo.users", new[] { "city_id" });
            DropIndex("dbo.payinto_histories", new[] { "bank_id" });
            DropIndex("dbo.payinto_histories", new[] { "user_id" });
            DropTable("dbo.withdrawl_histories");
            DropTable("dbo.rent_transaction");
            DropTable("dbo.games");
            DropTable("dbo.player_game");
            DropTable("dbo.payment_gateways");
            DropTable("dbo.info_players");
            DropTable("dbo.dou_histories");
            DropTable("dbo.comments");
            DropTable("dbo.cities");
            DropTable("dbo.users");
            DropTable("dbo.payinto_histories");
            DropTable("dbo.banks");
        }
    }
}
