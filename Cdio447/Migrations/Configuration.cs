﻿namespace Cdio447.Migrations
{
    using System;
    using System.Web.Hosting;
    using System.IO;
    using System.Text;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using Fizzler.Systems.HtmlAgilityPack;
    using HtmlAgilityPack;
    using System.Linq;
    using Cdio447.Models;
    using Cdio447.Supports;
    using System.Web;
    using System.Data.Entity.Validation;
    using System.Net;

    internal sealed class Configuration : DbMigrationsConfiguration<DBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DBContext context)
        {

            /*
            var html = @"https://playerduo.com/";
            HtmlWeb web = new HtmlWeb();
            var document = web.LoadFromBrowser(html);
            var node = document.DocumentNode.QuerySelectorAll(".dropdown-menu-wrap .user-info .info p:first-child").ToList().Count.ToString();
            */

            string[] arr_cities = new string[] { "An Giang", "Bà Rịa - Vũng Tàu", "Bạc Liêu", "Bắc Kạn", "Bắc Giang","Bắc Ninh", "Bến Tre", "Bình Dương", "Bình Định",
            "Bình Phước", "Bình Thuận","Cà Mau", "Cao Bằng","Cần Thơ", "Đà Nẵng", "Đắk Lắk","Đắk Nông","Đồng Nai","Đồng Tháp","Điện Biên","Gia Lai","Hà Giang",
            "Hà Nam", "Hà Nội", "Hà Tĩnh","Hải Dương","Hải Phòng", "Hòa Bình","Hậu Giang","Hưng Yên","Thành phố Hồ Chí Minh","Khánh Hòa","" +
            "Kiên Giang", "Kon Tum","Lai Châu","Lào Cai","Lạng Sơn","Lâm Đồng","Long An","Nam Định","Nghệ An","Ninh Bình","Ninh Thuận","Phú Thọ",
            "Phú Yên","Quảng Bình","Quảng Nam","Quảng Ngãi","Quảng Ninh","Quảng Trị","Sóc Trăng","Sơn La","Tây Ninh",
            "Thái Bình","Thái Nguyên","Thanh Hóa","Thừa Thiên - Huế","Tiền Giang","Trà Vinh","Tuyên Quang","Vĩnh Long",
            "Vĩnh Phúc","Yên Bái"
            };
            for (int i = 0; i < arr_cities.Length; i++)
            {
                cities city = new cities();
                city.name = arr_cities[i];
                context.cities.Add(city);
                context.SaveChanges();
            }

            string[] arr_games = new string[] { "Liên Minh Huyền Thoại", "PUBG", "PUBG Mobile","CS:GO", "GTA V", "Auto Chess",
                "Fortnite Battle Royale", "Fifa Online 4", "Human Fall Flat", "Apex Legend", "Rules Of Survival", "Liên Quân Mobile"
            };
            for (int i = 0; i < arr_games.Length; i++)
            {
                games game = new games();
                game.name = arr_games[i];
                context.games.Add(game);
                context.SaveChanges();
            }
            string[] arr_banks = new string[] { "ACB","Agribank","ABBank", "BIDV", "Đông Á",
                "Eximbank","JMB","Nam Á", "Sacombank","Sài Gòn Bank","SHB Bank","Techcombank","TP Bank","Vietcombank", "VP Bank"
            };
            string[] arr_bank_payin = new string[] { "ACB", "AGB", "ABB", "BIDV", "DAB", "EXB",
                "MB","NAB","STB", "SGCB","SHB","TCB","TPB","VCB", "VPB"
            };
            string[] arr_bank_imgs = new string[] { "acb.png", "agribank.png","abbank.png", "bidv.png", "donga.png", "eximbank.png",
                "jmb.png","nama.png","sacombank.png","saigonbank.png","shb.png","techcombank.png","tpbank.png","vietcombank.png", "vpbank.png"
            };

            for (int i = 0; i < arr_banks.Length; i++)
            {
                banks bank = new banks();
                bank.name = arr_banks[i].ToUpper();
                bank.image = arr_bank_imgs[i];
                bank.shortNamePayin = arr_bank_payin[i];
                bank.description = arr_banks[i];
                context.banks.Add(bank);
                context.SaveChanges();
            }
            //admin
            users userAdmin = new users();
            userAdmin.user_name = "admin";
            userAdmin.password = "e10adc3949ba59abbe56e057f20f883e";
            userAdmin.display_name = "Admin";
            userAdmin.email = "admin@gmail.com";
            userAdmin.role = 1;
            userAdmin.gender = 1;
            userAdmin.city_id = 1;
            userAdmin.phone = "0764033070";
            userAdmin.avatar = "admin.png";
            userAdmin.total_time_rent = 0;
            userAdmin.current_money = 1000000000;
            context.users.Add(userAdmin);
            context.SaveChanges();

            //users
            Random rd_2 = new Random();
            string path = "E:/DTU/CDIO447/cdio-447/Cdio447/Supports/crawler.html";
            var document = new HtmlDocument();
            document.Load(path);
            var getDocument = document.DocumentNode;
            var getList = getDocument.QuerySelectorAll(".player-information-card-wrap").ToList();

            try
            {
                int i = 2;
                foreach (var item in getList)
                {
                    var User_name = item.QuerySelector(".player-information .player-name > a").Attributes["href"].Value.ToString();
                    User_name = User_name.Replace("/", "").ToString();
                    String Pass_word = "e10adc3949ba59abbe56e057f20f883e";
                    var Display_name = item.QuerySelector(".player-information .player-name > a").InnerText.ToString();
                    var Email = User_name + rd_2.Next(1900, 2000).ToString() + "@gmail.com";
                    RandomDigits rd = new RandomDigits();
                    String Phone = rd.StartRandom();
                    int city_id = rd_2.Next(1, 63);
                    var Avatar = User_name + ".jpg";
                    decimal Current_money = 5000000;
                    users user = new users();
                    user.user_name = User_name;
                    user.password = Pass_word;
                    user.display_name = Display_name;
                    user.email = Email;
                    user.role = 2;
                    user.gender = (short)rd_2.Next(1, 3);
                    user.city_id = rd_2.Next(1, 63);
                    user.phone = Phone;
                    user.avatar = Avatar;
                    user.current_money = Current_money;
                    if (i >= 41)
                    {
                        user.status = 0;//chua co thong tin
                        user.total_time_rent = rd_2.Next(20, 100);
                    }
                    else
                    {
                        user.status = 1;//danh ranh
                        user.total_time_rent = rd_2.Next(500, 1000);
                    }
                    context.users.Add(user);
                    context.SaveChanges();
                    i += 1;
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            for (int i = 2; i <= 40; i++)
            {
                player_game pl = new player_game();
                pl.user_id = i;
                pl.game_id = rd_2.Next(1, 13);
                context.player_game.Add(pl);
                context.SaveChanges();
            }
            string[] info_player_titles = new string[] {
                "",
                "Thuê mình đi, ahihi",
                "Nhận chơi game xuyên màn đêm",
                "Tới luôn tới luôn =))))",
                "Một mình em chấp hết :))PUBG PC",
                "Game Gì Cũng chơi, chơi gì cũng tạ",
                "Vô danh tiểu tốt",
                "Chơi gì cũng pro nèeeeeee",
                "Tay mình to lắm thuê mình đi"
            };
            string[] links = new string[] {
                "https://www.facebook.com/",
                "https://www.instagram.com/",
                "https://www.messenger.com/",
                "aT9hx-NhKF0",
            };
            string[] info_player_tags = new string[] {
                "",
                "PUBG-LOL-Autochess",
                "PUBG-HumanFallFlat-LoL",
                "pubg-lmht-gtv5",
                "autochess-human-ark",
                "pubg mobile-tâm sự chém gió-ark",
            };
            string descriptionss = "Các game thủ chuyên nghiệp thường chơi trò chơi điện tử vì mục đích kiếm tiền hoặc để săn các giải thưởng chơi game. Họ thường là những người chơi game có kĩ năng tốt hoặc xuất sắc." +
                " Những người như vậy thường nghiên cứu kỹ lưỡng trò chơi để làm chủ nó và thường chơi trong các cuộc thi thể thao điện tử (eSports) chẳng hạn như Liên minh Huyền Thoại, DOTA 2,... " +
                "Một gamer chuyên nghiệp cũng có thể là một loại game thủ khác, chẳng hạn như Hardcore Gamer, nếu anh ta đáp ứng các tiêu chí bổ sung cho loại game thủ đó. Tại các quốc gia Châu Á";
            decimal[] info_player_amount_in_ones = new decimal[] {
                50000,
                25000,
                10000,
                35000,
                100000,
                200000,
                350000,
                1000000,
            };
            for (int i = 2; i <= 40; i++)
            {
                info_players if_player = new info_players();
                if_player.user_id = i;
                if_player.title = info_player_titles[rd_2.Next(1, 9)];
                if_player.description = descriptionss;
                if_player.link_fb = links[0];
                if_player.link_insta = links[1];
                if_player.link_messenger = links[2];
                if_player.link_youtube = links[3];
                if_player.total_time = rd_2.Next(500, 2000);
                if_player.amount_in_one = info_player_amount_in_ones[rd_2.Next(0, 7)];
                if_player.tag = info_player_tags[rd_2.Next(1, 6)];
                context.info_players.Add(if_player);
                context.SaveChanges();
            }
            string[] arr_commemts = new string[] {
                "",
                "Toàn rình đòi chén user",
                "tay vừa vừa , hát khá hay . Yêu nuôn",
                "Dễ thương hiền lành dễ tin người vl. Hihi",
                "Gật gật gật gật .... !",
                "Good",
                "vui ve hoa dong",
                "tốt cute",
                "vui tính, nhây, đánh tốt nói chung là đáng thuê :)",
                "Dễ thương siêu kute hết mình gánh user <3",
                "hay + dễ thương ... 1 khi đã game cùng thì ko phải nói lên 2 từ hối tiếc ...! player mà các bạn nên rent thử để cảm nhận",
                "qua oke",
            };
            for (int i = 1; i <= 300; i++)
            {
                comments cmt = new comments();
                cmt.user_id_cmt = rd_2.Next(40, 47);
                cmt.user_id_rcmt = rd_2.Next(2, 39);
                cmt.contents = arr_commemts[rd_2.Next(1, 5)];
                cmt.number_star = (short)rd_2.Next(3, 6);
                string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                DateTime date = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm:ss", null);
                cmt.created_at = date;
                context.comments.Add(cmt);
                context.SaveChanges();
            }
            //add dou_histories
            for (int i = 1; i <= 50; i++)
            {
                dou_histories dh = new dou_histories();
                dh.user_id_rent = rd_2.Next(40, 47);
                var user_id_dou = rd_2.Next(2, 39);
                dh.user_id_dou = user_id_dou;
                info_players getAmountOne = context.info_players.SingleOrDefault(x => x.user_id == user_id_dou);
                var total_time = rd_2.Next(1, 4);
                dh.total_time = total_time;
                dh.total_amount = (decimal)total_time * (decimal)getAmountOne.amount_in_one;
                string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                DateTime date = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm:ss", null);
                //dh.created_at = Convert.ToDateTime(datetime);
                dh.created_at = date;
                context.dou_histories.Add(dh);
                context.SaveChanges();
            }
        }
    }
}
