﻿$(document).ready(function () {
    var getDetail = {
        init: function () {
            getDetail.registerEvent();
        },
        registerEvent: function () {
            $('.btn-detail').off('click').on('click', function () {
                $('#modalAddUpdate').modal('show');
                var id = $(this).data('id');
                getDetail.loadDetail(id);
            });
        },
        loadDetail: function (id) {
            $.ajax({
                url: '/Admin/ManUsers/GetDetail',
                data: {
                    id: id
                },
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    if (response.status == true) {
                        var data = response.data;
                        $('#txtUserName').val(data.user_name);
                        $('#txtDisplayName').val(data.display_name);
                        $('#txtGender').val(data.gender == 1 ? "Nam":"Nữ");
                        $('#txtCity').val(data.namecity);
                        $('#txtEmail').val(data.email);
                        $('#txtPhone').val(data.phone);
                        $('#txtMoney').val(data.totalcurrency);
                    }
                    else {
                        bootbox.alert(response.message);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
    }
    getDetail.init();
});