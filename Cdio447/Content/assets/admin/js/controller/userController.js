﻿var user = {
    init: function () {
        user.registerEvents();
    },
    registerEvents: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/ManUsers/ChangeStatus",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.status == 3) {
                        btn.text('Kích hoạt');
                        location.reload()
                    }
                    else {
                        btn.text('Khoá');
                        location.reload()
                    }
                }
            });
        });
    }
}
user.init();