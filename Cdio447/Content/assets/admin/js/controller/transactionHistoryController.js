﻿
var transactionHistoryController = {
    init: function () {
        transactionHistoryController.registerEvent();
    },
    registerEvent: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/Admin/TransactionHistory/changeStatus",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.status == 2) {
                        btn.text('Xác nhận');
                        location.reload()
                    }
                }
            });
        });
    },
    
}
transactionHistoryController.init();