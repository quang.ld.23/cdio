﻿$(document).ready(function () {
    $('.single-item').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        autoplaySpeed: 4000,
        autoplay: true,
        cssEase: 'linear'
    });
    //select city is a chosen
    $('#select_city').chosen({
        max_selected_options: 5,
        no_results_text: "Không tìm thấy thành phố!!!"
    });
    var rangerSlider = $("#ranger-slider");
    var rangerValueMinText = $("#ranger-value-min-text");
    var rangerValueMaxText = $("#ranger-value-max-text");
    rangerSlider.slider({
        max: 1000000,
        min: 10000,
        step: 10000,
        values: [10000, 1000000],
        range: true,
        slide: function (event, ui) {
            $("#ranger-value-min-text").html(ui.values[0]);
            $("#ranger-value-max-text").html(ui.values[1]);
        },
        stop: function (event, ui) {
            $("#ranger-value-min").val(ui.values[0]);
            $("#ranger-value-max").val(ui.values[1]);
        }
    });
    $("#ranger-value-min").val(rangerSlider.slider('values', 0));
    $("#ranger-value-max").val(rangerSlider.slider('values', 1));
    rangerValueMinText.html(rangerSlider.slider('values', 0));
    rangerValueMaxText.html(rangerSlider.slider('values', 1));
    //button click delete
    $('#delete_gender').click(function () {
        //$('input[name="gender"]').attr('checked', false);
        $('input[name="gender"]').prop('checked', false);
    });
    $('#delete_name').click(function () {
        $("input[name='name']").val("");
    });
    $('#delete_tag').click(function () {
        $("input[name='tag']").val("");
    });

    //convert to vnd
    function change_vnd(arg, suffix) {
        var str = arg.toString();
        var length = str.length;
        var counter = 3;
        var result = "";
        while (length - counter >= 0) {
            var con = str.substr(length - counter, 3);
            result = "," + con + result;
            counter = counter + 3;
        }
        var con2 = str.substr(0, 3 - (counter - length));
        result = con2 + result;
        if (result.substr(0, 1) == ",") {
            result = result.substr(1, result.length + 2);
        }
        result = result + suffix;
        return result;
    }
    //render Function for renderjs
    var myHelpers = { format: change_vnd };
    //call loader hide
    $('#box-loader').hide();
    //btn filter click
    $("#btnFilter").click(function () {
        $('#filter_title').css('display', 'block');
        var getGender = (typeof $('input[type="radio"][name="gender"]:checked').val() === 'undefined') ? 'null' : $('input[type="radio"][name="gender"]:checked').val();
        var getArrCities = ($('#select_city').chosen().val().length === 0) ? ["null"] : $('#select_city').chosen().val();
        var getName = ($("#name").val().length == 0) ? "null" : $("#name").val();
        var getTag = ($("#tag").val().length == 0) ? "null" : $("#tag").val();
        var jsonData = {
            gender: getGender,
            arr_cities: getArrCities,
            name: getName,
            tag: getTag,
            max_price: $("#ranger-value-max").val(),
            min_price: $("#ranger-value-min").val()
        };
        $.ajax({
            url: "/Home/Filter",
            type: 'POST',
            data: JSON.stringify(jsonData),
            contentType: 'application/json;charset=UTF-8',
            dataType: "json",
            success: function (response) {
                $('#box-loader').show();
                setTimeout(function () {
                    if (response.length > 0) {
                        $('#filter_title h3').text("Kết quả tìm kiếm theo bộ lọc");
                        var template = $.templates("#scriptRender");
                        var htmlOutput = template.render(response, myHelpers);
                        $('#filter_list > .container > .row').html(htmlOutput);
                    } else {
                        $('#filter_title h3').html("Kết quả tìm kiếm theo bộ lọc:</br> <span style='color: #f22a16'>Không tìm thấy kết quả tương ứng</span>");
                        var template = $.templates("#scriptRender");
                        var htmlOutput = template.render(response);
                        $('#filter_list > .container > .row').html(htmlOutput);
                    }
                }, 3000);
                setTimeout(function () {
                    $('#box-loader').hide();
                }, 3000);
            },
            error: function () {
                alert("Có lỗi xảy ra");
            },
            complete: function () {
                $('.modal').modal('toggle');
            }
        });
    });
    //get Bank_id
    $('input[type=radio][name=banks]').on('change', function () {
        $('input[type=hidden][name=bank_id]').val($(this).attr('id'));
    });
    //Not allow user enter amount with first "0"
    var getValueSelected = $("#select_bankId option:selected").val();
    if (getValueSelected != 0 || getValueSelected != "0") {
        $('input[type=hidden][id=bankIDPW]').val(getValueSelected);
    }
    $("#select_bankId").on('change click', function () {
        $('input[type=hidden][id=bankIDPW]').val($(this).children("option:selected").val());
    });

    //change amount to VND in payinto and withdraw
    const regexCheckAmount = /^[1-9][0-9]*$/;
    var value = $("input[type=hidden][id=amountPayWd]").val();
    value = value + "";
    if (value.search(",") != -1 || value.indexOf(",") != -1) {
        value = value.split(",").join("");
    }
    if (regexCheckAmount.test(value) == true) {
        var newValue = change_vnd(value, "");
        $("input[type=text][id=amount_convert]").val(newValue);
    } else {
        $("input[type=text][id=amount_convert]").val(value);
    }
    function eventConvertVND(value) {
        value = value.toString();
        if (value.search(",") != -1 || value.indexOf(",") != -1) {
            value = value.split(",").join("");
        }
        if (regexCheckAmount.test(value) == true) {
            var newValue = change_vnd(value, "");
            $("input[type=text][id=amount_convert]").val(newValue);
            if (newValue.search(",") != -1 || newValue.indexOf(",") != -1) {
                var newValue2 = newValue.split(",").join("");
                $("input[type=hidden][id=amountPayWd]").val(newValue2);
            }
        } else {
            $("input[type=hidden][id=amountPayWd]").val(value);
        }
    }
    $("input[type=text][id=amount_convert]").on('change paste keyup keydown', function () {
        var value = $(this).val();
        eventConvertVND(value);
    });
    //select game is a chosen
    $('#select_game').chosen({
        placeholder_text_single: "Chọn các tựa game",
        no_results_text: "Không tìm thấy tựa game!!!"
    });
    //get All game_id in user infoplayer
    var array_Game = [];
    $("input[type=hidden][class=gameIdrender]").each(
        function (index) {
            array_Game.push($(this).val());
        }
    );
    if (array_Game.length > 0)
        $('#select_game').val(array_Game).trigger('chosen:updated');
    $("#select_game").chosen().change(function () {
        var array_Game = $(this).chosen().val();
        $("input[type=hidden][class=gameIdrender]").remove();
        //repeat render
        for (var i = 0; i < array_Game.length; i++) {
            var str_render = '<input class="gameIdrender"' +
            'data-val="true" data-val-number="The field Int32 must be a number." data-val-required="The Int32 field is required."' +
                'id="game_id_' + i + '_"' + 'name="game_id[' + i + ']"' + 'type="hidden" value="' + array_Game[i] + '">';
            $(".chosen-container.chosen-container-multi").after(str_render);
        }
    });
});
