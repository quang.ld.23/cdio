﻿$(document).ready(function () {
    //global ID User
    var urlUserDetail = window.location.href;
    var id = urlUserDetail.substring(urlUserDetail.lastIndexOf('/') + 1);

    function change_vnd(arg, suffix) {
        var str = arg.toString();
        var length = str.length;
        var counter = 3;
        var result = "";
        while (length - counter >= 0) {
            var con = str.substr(length - counter, 3);
            result = "," + con + result;
            counter = counter + 3;
        }
        var con2 = str.substr(0, 3 - (counter - length));
        result = con2 + result;
        if (result.substr(0, 1) == ",") {
            result = result.substr(1, result.length + 2);
        }
        result = result + suffix;
        return result;
    }
    //change time rent
    $('#timeRent').on('change', function () {
        var getPrice = $('#getAmount').val();
        getPrice = parseInt(getPrice) * parseInt($(this).val());
        getPrice = change_vnd(getPrice, " đ");
        $('.priceRent').text(getPrice);
    });

    //click rent
    $("#rentPlayer").on("click", function () {
        var jsonPostData = {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
            userIdDou: id,
            message: $('#messageRent').val(),
            totalTime: $('#timeRent').val(),
        };
        $.ajax({
            url: "/User/AjaxRentPlayer",
            type: 'POST',
            data: jsonPostData,
            dataType: "json",
            success: function (response) {
                if (response == 1) {
                    $('.modal').modal('toggle');
                    alert("Thuê thành công");
                    setTimeout(function () {
                        location.reload();
                    }, 700);
                } else if (response == 0) {
                    alert("Không thể giao dịch với chính mình");
                }
                else if (response == 2) {
                    alert("Số tiền không đủ để thực hiện giao dịch");
                }
                else {
                    alert("Có lỗi xảy ra");
                }
            },
            error: function () {
                console.log("Có lỗi xảy ra");
            },
        });
    });
    //coutdown
    $(function () {
        getTimeRent();
    });
    function getTimeRent() {
        var jsonPostData = {
            __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
            idUser: id
        };
        $.ajax({
            url: "/User/getTimeRent",
            type: 'POST',
            data: jsonPostData,
            dataType: "json",
            success: function (response) {
                if (typeof response === 'object') {
                    console.log(response);
                    $('.time-rent').addClass("d-block");
                    $('#h').text(response[0]);
                    $('#m').text(response[1]);
                    $('#s').text(response[2]);
                    startTime();
                }
                else if (response == 1) {
                    window.location.href = urlUserDetail;
                } else {
                    $('.time-rent').addClass("d-none");
                }
            },
            error: function () {
                alert("Có lỗi xảy ra");
            },
        });
    }

    var h = null;
    var m = null;
    var s = null;

    var timeoutRent = null; // Timeout

    function startTime() {
        if (h === null) {
            h = parseInt(document.getElementById('h').innerText);
            m = parseInt(document.getElementById('m').innerText);
            s = parseInt(document.getElementById('s').innerText);
        }
        if (s === -1) {
            m -= 1;
            s = 59;
        }
        if (m === -1) {
            h -= 1;
            m = 59;
        }
        if (h == -1) {
            clearTimeout(timeoutRent);
            window.location.href = urlUserDetail;
            return false;
        }
        document.getElementById('h').innerText = h.toString();
        document.getElementById('m').innerText = m.toString();
        document.getElementById('s').innerText = s.toString();
        timeoutRent = setTimeout(function () {
            s--;
            startTime();
        }, 1000);
    }

    //edit tags
    var getTags = $('.tag > p').text().trim();
    if (getTags != "") {
        var arrs = getTags.split("-");
        for (var i = 0; i < arrs.length; i++) {
            $('.tag > .tag-append').append("<p>" + arrs[i] + "</p>");
        }
    }
    $('.tag > p').text("");
    $('.radio-rating').change(function () {
        $('#number_star').val(this.value);
    });

    //comment
    $("#btnComment").on("click", function () {
        var getContent = $("#textContentComment").val();
        var getNumberStar = $("#number_star").val();
        var userIdRcmt = id;
        var jsonPostData = {
            getContent: getContent,
            getNumberStar: getNumberStar,
            userIdRcmt: userIdRcmt
        };
        if (getContent == "" || getNumberStar == "" || getContent == null || getNumberStar == null) {
            alert("Vui lòng nhập thông tin đánh giá và chọn số sao");
        } else {
            $.ajax({
                url: "/User/Comment",
                type: 'POST',
                data: JSON.stringify(jsonPostData),
                contentType: 'application/json;charset=UTF-8',
                dataType: "json",
                success: function (response) {
                    switch (response) {
                        case 1:
                            alert("Không thể đánh giá cho chính bạn");
                            break;
                        case 2:
                            alert("Khổng thể đánh giá vì bạn chưa từng thuê người chơi này");
                            break;
                        case 3:
                            alert("Vui lòng nhập thông tin đánh giá và chọn số sao");
                            break;
                        case 4:
                            alert("Nội dung đánh giá nhiều hơn 15 ký tự");
                            break;
                        case 5:
                            alert("Đánh giá thành công");
                            setTimeout(function () {
                                window.location.href = urlUserDetail;
                            }, 200);
                            break;
                    }
                },
                error: function () {
                    alert("Có lỗi xảy ra");
                },
            });
        }
    });
    //paging comment
    $(function () {
        GetListComment(1);
    });
    $("body").on("click", ".Pager .page", function () {
        GetListComment(parseInt($(this).attr('page')));
    });
    function GetListComment(pageIndex) {
        var jsonPostData = {
            pageIndex: pageIndex,
            userId: id
        };
        $.ajax({
            type: "POST",
            url: "/User/AjaxPagingComment",
            data: JSON.stringify(jsonPostData),
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (response) {
                if (response != 1) {
                    //add count number_star for rendering comments;
                    $.views.helpers({
                        countNumberStar: function (count) {
                            var fieldsArray = [];
                            for (var i = 0; i < count; i++) {
                                fieldsArray.push({});
                            }
                            return fieldsArray;
                        }
                    });
                    var model = JSON.parse(response);
                    var template = $.templates("#scriptRenderComment");
                    var htmlOutput = template.render(model.getListComments);
                    $('.col-12 .info-comment-render').html(htmlOutput);
                    $(".Pager").ASPSnippets_Pager({
                        ActiveCssClass: "current",
                        PagerCssClass: "pager",
                        PageIndex: model.PageIndex,
                        PageSize: model.PageSize,
                        RecordCount: model.RecordCount
                    });
                } else {
                    $('.col-12 .info-comment-render').html("<h3 class='mb-0 pb-2' style='border-bottom: 1px solid #e74c3c'>Chưa có đánh giá</h3>");
                }
            },
            error: function () {
                alert("Có lỗi xảy ra trong quá trình đọc danh sách đánh giá");
            }
        });
    };
});