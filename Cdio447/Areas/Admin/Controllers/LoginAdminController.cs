﻿using Cdio447.Code;
using Cdio447.Model_Supports;
using Cdio447.Models;
using Cdio447.ViewModels;
using System.Web.Mvc;

namespace Cdio447.Areas.Admin.Controllers
{
    public class LoginAdminController : Controller
    {
        // GET: Admin/LoginAdmin
        [HttpGet]
        public ActionResult LoginAdmin()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[ValidateInput(false)]
        public ActionResult LoginAdmin(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var dao = new AccountModel();
                var result = dao.Login(model.UserName, model.PassWord);
                if (result == 1)// user là admin
                {
                    var user = dao.GetById(model.UserName);
                    var userSession = new UserSession();
                    userSession.UserName = user.user_name;
                    userSession.Role = user.role;
                    Session.Add(CommonConstants.USER_SESSION, userSession);

                    return RedirectToAction("Index", "HomeAdmin");
                }
                else if (result == 2)// là user
                {
                    ViewBag.Message = "Your account dont have access!";
                }
                else if (result == 0)
                {
                    ModelState.AddModelError("", "Sai tên đăng nhập hoặc mật khẩu.");
                }
                else if (result == -1)
                {
                    ModelState.AddModelError("", "Vui lòng nhập Tên đăng nhập và Mật khẩu.");
                }
            }
            else
            {
                ModelState.AddModelError("", "Vui lòng nhập Tên đăng nhập và Mật khẩu.");
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session[CommonConstants.USER_SESSION] = null;
            return RedirectToAction("LoginAdmin", "LoginAdmin");
        }

        [ChildActionOnly]
        public ActionResult Avatar_Amin()
        {
            var sesion = (UserSession)Session[CommonConstants.USER_SESSION];

            if (sesion != null)
            {
                int role = sesion.Role;
                if (role != 2)
                {
                    var avatar = new AccountModel().AvatarUser(sesion.UserName);

                    return PartialView(avatar);
                }
                else
                {
                    string name = "Username";
                    var avatar = new AccountModel().AvatarUser(name);
                    return PartialView(avatar);
                }
            }
            else
            {
                string name = "Username";
                var avatar = new AccountModel().AvatarUser(name);
                return PartialView(avatar);
            }
        }
    }
}