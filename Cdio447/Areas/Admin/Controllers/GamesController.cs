﻿using Cdio447.Model_Supports;
using Cdio447.Models;
using Cdio447.ViewModels;
using System.Net;
using System.Web.Mvc;

namespace Cdio447.Areas.Admin.Controllers
{
    public class GamesController :CheckloginController
    {
        private IGamesSupport _repository;

        public GamesController()
        : this(new GameSupport())
        {
        }

        public GamesController(IGamesSupport repository)
        {
            _repository = repository;
        }

        // GET: Admin/Games
        public ActionResult Index(string searchString)
        {
            var list = _repository.ListAllGames(searchString);
            return View(list);
        }

        // GET: Admin/Games/Create
        public ActionResult Create()
        {
            return View(new games());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(games games)
        {
            if (ModelState.IsValid)
            {
                if (_repository.CheckName(games))
                {
                    ModelState.AddModelError("name", "Tên đã được dùng");
                    return View(games);
                }
                _repository.InsertGame(games);

                return RedirectToAction("Index");
            }

            return View(games);
        }

        // GET: Admin/Games/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            games games = _repository.GamesByID(id);
            if (games == null)
            {
                return HttpNotFound();
            }
            return View(games);
        }

        // POST: Admin/Games/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(games games)
        {
            if (ModelState.IsValid)
            {
                _repository.UpdateGame(games);
                TempData["message"] = "Cập nhật thành công";
                return RedirectToAction("Index");
            }
            return View(games);
        }

        // GET: Admin/Games/Delete/5
        public ActionResult Delete(int id)
        {
            _repository.DeleteGame(id);
            return RedirectToAction("Index");
        }
    }
}