﻿using Cdio447.Areas.Admin.Models;
using Cdio447.Models;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Cdio447.Areas.Admin.Controllers
{
    public class TransactionHistoryController : CheckloginController
    {

        private DBContext db;

        public TransactionHistoryController()
        {
            db = new DBContext();
        }

        // GET: Admin/TransactionHistory
        public ActionResult Index()
        {
            return View();
        }

        //GET: Admin/Recharge
        public ActionResult Recharge(string searchString, int page = 1, int pageSize = 12)
        {
            IEnumerable<payinto_histories_View> list = (from a in db.payinto_histories
                                                        join b in db.users
                                                        on a.user_id equals b.id
                                                        join c in db.banks
                                                        on a.bank_id equals c.id
                                                        select new
                                                        {
                                                            user_name = b.user_name,
                                                            bank = c.name,
                                                            amount = a.amount,
                                                            oder_code = a.oder_code,
                                                            created_at = a.created_at
                                                        }).Select(u => new payinto_histories_View()
                                                        {
                                                            user_name = u.user_name,
                                                            bank = u.bank,
                                                            amount = (int)u.amount,
                                                            oder_code = u.oder_code,
                                                            created_at = u.created_at
                                                        });
            if (!string.IsNullOrEmpty(searchString))
            {
                list = list.Where(u => u.user_name.Contains(searchString));
            }
            list = list.ToList().ToPagedList(page, pageSize);
            return View(list);
        }

        //GET: Admin/Withdrawl
        public ActionResult Withdrawl(string searchString, short? status, int page = 1, int pageSize = 12)
        {
            IEnumerable<withdrawl_histories_View> list = (from a in db.withdrawl_histories
                                                          join b in db.users
                                                          on a.user_id equals b.id
                                                          join c in db.payment_gateways
                                                          on a.user_id equals c.user_id
                                                          select new
                                                          {
                                                              id = a.id,
                                                              user_name = b.user_name,
                                                              account_name = c.account_name,
                                                              account_code = c.account_code,
                                                              total = a.total,
                                                              status = a.status,
                                                              created_at = a.created_at.ToString()
                                                          }).Select(u => new withdrawl_histories_View()
                                                          {
                                                              id = u.id,
                                                              user_name = u.user_name,
                                                              account_name = u.account_name,
                                                              account_code = u.account_code,
                                                              total = (int)u.total,
                                                              status = u.status,
                                                              created_at = u.created_at
                                                          });
            if (!string.IsNullOrEmpty(searchString))
            {
                list = list.Where(u => u.user_name.Contains(searchString));
            }
            if (status == 1 || status == 2)
            {
                list = list.Where(u => u.status == status);
            }
            else
            {
                list = list.ToList().ToPagedList(page, pageSize);
            }
            list = list.ToList().ToPagedList(page, pageSize);
            return View(list);
        }

        [HttpPost]
        public JsonResult changeStatus(int id)
        {
            var data = change(id);
            return Json(new
            {
                status = data
            });
        }

        public short? change(int id)
        {
            var user = db.withdrawl_histories.Find(id);
            if (user.status != 2)
            {
                user.status = 2;
            }
            else
            {
                user.status = 1;
            }
            db.SaveChanges();
            return user.status;
        }

        
    }
}