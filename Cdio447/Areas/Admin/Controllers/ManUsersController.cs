﻿using Cdio447.Areas.Admin.Models;
using Cdio447.Model_Supports;
using PagedList;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cdio447.Supports;

namespace Cdio447.Areas.Admin.Controllers
{
    public class ManUsersController : CheckloginController
    {
        private IManUsers res;

        public ManUsersController()
           : this(new ManUsers())
        {
        }

        public ManUsersController(IManUsers repository)
        {
            res = repository;
        }

        // GET: Admin/ManUsers
        public ActionResult Index(string searchString, int page = 1, int pageSize = 12)
        {
            if (searchString != null)
            {
                var list = res.Listallusers().Where(u => u.user_name.StartsWith(searchString) || searchString == null).OrderBy(u => u.user_name).ToPagedList(page, pageSize);
                return View(list);
            }
            else
            {
                var list = res.Listallusers().OrderBy(u => u.user_name).ToPagedList(page, pageSize);
                return View(list);
            }
        }

        // GET: Admin/ManUsers/Create
        public ActionResult Create()
        {
            SetViewbag();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(userView user)
        {
            HttpPostedFileBase fileUpload = Request.Files["fileUpload"];

            if (ModelState.IsValid)
            {
                //check user_name already exist
                if (res.checkUser(user) || res.checkDisplayName(user) || res.checkPhone(user) || res.checkEmail(user))
                {
                    if (res.checkUser(user))
                    {
                        ModelState.AddModelError("user_name", "Tên đã được dùng");
                    }
                    //check display_name already exist
                    if (res.checkDisplayName(user))
                    {
                        ModelState.AddModelError("display_name", "Tên đã được dùng");
                    }

                    //check phone_number already exist
                    if (res.checkPhone(user))
                    {
                        ModelState.AddModelError("phone", "Số điện thoại đã được dùng");
                    }

                    //check email already exist
                    if (res.checkEmail(user))
                    {
                        ModelState.AddModelError("email", "Email đã được dùng");
                    }
                    ModelState.AddModelError("", "Thêm mới không thành công");
                    SetViewbag();
                    return View(user);
                }
                string avatar = null;
                if (fileUpload != null)
                {
                    var fileName = Path.GetFileName(fileUpload.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/assets/img"), fileName);
                    if (fileName != "")
                    {
                        fileUpload.SaveAs(path);
                        avatar = fileName;
                    }
                }

                res.insertUser(user, avatar);

                SetAlert("Thêm thành công", "success");
                return RedirectToAction("Index");
            }
            SetViewbag();
            return View(user);
        }

        public void SetViewbag()
        {
            //DropList City
            ViewBag.CityID = new SelectList(res.listciy(), "id", "name");
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            var rsl = res.changeStatus(id);
            return Json(new
            {
                status = rsl
            });
        }

        [HttpGet]
        public JsonResult GetDetail(int id)
        {
            var detail = res.GetuserbyID(id);
            detail.totalcurrency = FunctionSupport.convertVND(detail.current_money.GetValueOrDefault(), " -VND");
            return Json(new
            {
                data = detail,
                status = true
            }, JsonRequestBehavior.AllowGet);
        }

        protected void SetAlert(string message, string type)
        {
            TempData["AlertMessage"] = message;
            if (type == "success")
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == "warning")
            {
                TempData["AlertType"] = "alert-warning";
            }
            else if (type == "error")
            {
                TempData["AlertType"] = "alert-danger";
            }
        }
    }
}