﻿using Cdio447.Code;
using System.Web.Mvc;
using System.Web.Routing;

namespace Cdio447.Areas.Admin.Controllers
{
    public class CheckloginController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];

            if (session == null)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "LoginAdmin", action = "LoginAdmin", }));
            }
            else
            { //////    NOTE QUAN TRONG
                int role = session.Role; // kiểm tra role (quyen)
                if (role == 1)
                    base.OnActionExecuting(filterContext);
                else
                    filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "LoginAdmin", action = "LoginAdmin", }));
            }
        }
    }
}