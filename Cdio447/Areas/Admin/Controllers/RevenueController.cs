﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cdio447.Models;
using Cdio447.Areas.Admin.Models;
using System.Data.Entity;

namespace Cdio447.Areas.Admin.Controllers
{
    public class RevenueController : CheckloginController
    {
        private DBContext db = null;
        public RevenueController()
        {
            db = new DBContext();
        }
        // GET: Admin/Revenue
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Getinfo()
        {
            var sevenDayAgo = DateTime.Now.AddDays(-6);

            Revenue_View getTotal= new Revenue_View();
            
            var gettran = db.dou_histories.Where(x => DbFunctions.TruncateTime(x.created_at) >= DbFunctions.TruncateTime(sevenDayAgo)).Distinct().Count(u => u.id != 0);
            var getamount = db.dou_histories.Where(x => DbFunctions.TruncateTime(x.created_at) >= DbFunctions.TruncateTime(sevenDayAgo)).Sum(u => u.total_amount);

            getTotal.totalAmount = getamount / 10;
            getTotal.totaltransaction = gettran;
            ViewBag.totalSevenDay = (getTotal== null) ? null : getTotal;
            return PartialView();
        }

        [HttpPost]
        public JsonResult GetRevenue()
        {
            List<DateTime> arrSevenday = new List<DateTime>();
            List<Revenue_View> arrData = new List<Revenue_View>();
            List<string> arrSevendayConvert = new List<string>();
            List<string> arrDataConvert = new List<string>();
            //get last seven day
            for (int i = 0; i <= 6; i++)
            {
                DateTime nDaysAgo;
                if (i == 0)
                {
                    nDaysAgo = DateTime.Today.AddDays(i);
                }
                else
                {
                    nDaysAgo = DateTime.Today.AddDays(-i);
                }
                arrSevenday.Add(nDaysAgo);
            }
            //get data last seven day
            for (int i = 0; i <= 6; i++)
            {
                var convertDate = arrSevenday[i];
                var getData = db.dou_histories.
                Where(x => DbFunctions.TruncateTime(x.created_at) == DbFunctions.TruncateTime(convertDate)).
                GroupBy(x => x.created_at).
                Select(x => new Revenue_View { totalAmount = x.Sum(a => a.total_amount)/10 }).
                OrderBy(x => x.totalAmount).FirstOrDefault();
                if (getData == null)
                {
                    Revenue_View newIf = new Revenue_View();
                    newIf.totalAmount = 0;
                    arrData.Add(newIf);
                }
                else
                {
                    arrData.Add(getData);
                }
            }
            foreach (var get in arrSevenday)
            {
                arrSevendayConvert.Add(get.ToShortDateString());
            }
            foreach (var get in arrData)
            {
                arrDataConvert.Add(get.totalAmount.ToString());
            }
            var merge = arrSevendayConvert.Concat(arrDataConvert);
            return Json(merge);
        }
    }
}