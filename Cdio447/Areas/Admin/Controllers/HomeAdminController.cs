using System.Web.Mvc;
﻿using Cdio447.Code;
using Cdio447.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cdio447.Areas.Admin.Models;
using System.Data.Entity;
using Cdio447.Models;

namespace Cdio447.Areas.Admin.Controllers
{
    public class HomeAdminController : CheckloginController
    {
        private DBContext db = null;
        public HomeAdminController()
        {
            db = new DBContext();
        }
        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult getInfor() {
            homepage data = new homepage();
            var today = DateTime.Now;
            var gettotalamount = db.dou_histories
                                   .Sum(x => x.total_amount);
            var gettotaluser = db.users
                                 .Where(x => x.status == 1)
                                 .Count(x => x.id != 0);
            var gettran = db.withdrawl_histories
                            .Where(x => x.status == 2)
                            .Distinct().Count(u => u.id != 0);
            var gettrantoday = db.dou_histories
                                 .Where(x => DbFunctions.TruncateTime(x.created_at) >= DbFunctions.TruncateTime(today))
                                 .Count(u => u.id != 0);
            data.totalamount = gettotalamount / 10;
            data.totaluserOnline = gettotaluser;
            data.withdraw = gettran;
            data.totaltransactionToday = gettrantoday;
            ViewBag.Homepage = (data == null) ? null : data;
            return PartialView();
        }

        [HttpPost]
        public JsonResult GetRevenue()
        {
            List<DateTime> arrSevenday = new List<DateTime>();
            List<Revenue_View> arrData = new List<Revenue_View>();
            List<string> arrSevendayConvert = new List<string>();
            List<string> arrDataConvert = new List<string>();
            //get last seven day
            for (int i = 0; i <= 6; i++)
            {
                DateTime nDaysAgo;
                if (i == 0)
                {
                    nDaysAgo = DateTime.Today.AddDays(i);
                }
                else
                {
                    nDaysAgo = DateTime.Today.AddDays(-i);
                }
                arrSevenday.Add(nDaysAgo);
            }
            //get data last seven day
            for (int i = 0; i <= 6; i++)
            {
                var convertDate = arrSevenday[i];
                var getData = db.dou_histories.
                Where(x => DbFunctions.TruncateTime(x.created_at) == DbFunctions.TruncateTime(convertDate)).
                GroupBy(x => x.created_at).
                Select(x => new Revenue_View { totalAmount = x.Sum(a => a.total_amount) / 10 }).
                OrderBy(x => x.totalAmount).FirstOrDefault();
                if (getData == null)
                {
                    Revenue_View newIf = new Revenue_View();
                    newIf.totalAmount = 0;
                    arrData.Add(newIf);
                }
                else
                {
                    arrData.Add(getData);
                }
            }
            foreach (var get in arrSevenday)
            {
                arrSevendayConvert.Add(get.ToShortDateString());
            }
            foreach (var get in arrData)
            {
                arrDataConvert.Add(get.totalAmount.ToString());
            }
            var merge = arrSevendayConvert.Concat(arrDataConvert);
            return Json(merge);
        }
    }
}