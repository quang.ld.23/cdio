﻿using Cdio447.Areas.Admin.Models;
using Cdio447.Areas.Admin.Models.Infrastructure;
using PagedList;
using System.Linq;
using System.Web.Mvc;

namespace Cdio447.Areas.Admin.Controllers
{
    public class DouHistoriesController : CheckloginController
    {
        private IHistoriesdou res;

        public DouHistoriesController()
           : this(new Historiesdou())
        {
        }

        public DouHistoriesController(IHistoriesdou reponsitory)
        {
            res = reponsitory;
        }

        // GET: Admin/DouHistories
        public ActionResult Index(string searchStringDou, string searchStringRent, int page = 1, int pageSize = 12)
        {
            var list = res.lisDou();
            if (!string.IsNullOrEmpty(searchStringDou))
            {
                list = list.Where(u => u.user_name_dou.StartsWith(searchStringDou));
            }
            if (!string.IsNullOrEmpty(searchStringRent))
            {
                list = list.Where(u => u.user_name_rent.StartsWith(searchStringRent));
            }
            list = list.OrderBy(x => x.user_name_dou).ToPagedList(page, pageSize);
            return View(list);
        }
    }
}