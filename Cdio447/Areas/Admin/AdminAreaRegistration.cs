﻿using System.Web.Mvc;

namespace Cdio447.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            //context.MapRoute(

            //     "Admin_default",
            //   "Admin/{controller}/{action}/{id}",
            //   new { controller = "HomeAdmin", action = "Index", id = UrlParameter.Optional });

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Cdio447.Areas.Admin.Controllers" }
            );
        }
    }
}