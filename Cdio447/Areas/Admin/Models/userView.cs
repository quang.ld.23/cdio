﻿using Cdio447.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Cdio447.Areas.Admin.Models
{
    public class userView
    {


        [Required(ErrorMessage = "Không được bỏ trống")]
        [Display(Name = "Tên tài khoản")]
        [RegularExpression(@"^[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*$", ErrorMessage = "Tên tài khoản không đúng định dạng")]
        [StringLength(16,ErrorMessage ="Tên tài khoản phải từ 4-16 ký tự",MinimumLength =4)]
        public string user_name { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [Display(Name = "Mật khẩu")]
        [RegularExpression(@"^(?=(?:[a-zA-Z0-9]*[a-zA-Z]){2})(?=(?:[a-zA-Z0-9]*\d){2})[a-zA-Z0-9]{8,16}$", ErrorMessage = "Mật khẩu phải từ 8-16 ký tự, không chứa ký tự đặc biệt")]
        [StringLength(100)]
        public string password { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [Display(Name = "Nhập lại mật khẩu")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("password", ErrorMessage = "Mật khẩu không khớp")]
        public string ConfirmPassowrd { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [Display(Name = "Tên đại diện")]
        [RegularExpression(@"^[^:>#*]+|([^:>#*][^>#*]+[^:>#*])$", ErrorMessage = "Tên đại diện không đúng định dạng")]
        [StringLength(100)]
        public string display_name { get; set; }

        [Display(Name = "Số điện thoại")]
        [Required(ErrorMessage = "Không được bỏ trống")]
        [RegularExpression(@"^\d{3}\d{3}\d{4}$", ErrorMessage = "Số điện thoại phải là 10 chữ số")]
        [StringLength(50)]
        public string phone { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(50)]
        [Display(Name = "Email")]
        [RegularExpression(@"^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$", ErrorMessage = "Địa chỉ Email không hợp lệ")]
        public string email { get; set; }

        [Display(Name = "Loại tài khoản")]
        public short role { get; set; }

        [Display(Name = "Giới tính")]
        public short? gender { get; set; }

        [Display(Name = "Thành phố")]
        public int city_id { get; set; }

        [Display(Name = "Ảnh đại diện")]
        [ValidateFileAttribute]
        public HttpPostedFileBase avatar { get; set; }

    }
}