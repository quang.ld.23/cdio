﻿using System.ComponentModel.DataAnnotations;

namespace Cdio447.Areas.Admin.Models
{
    public class withdrawl_histories_View
    {
        public int id { get; set; }

        public string user_name { get; set; }

        public string account_name { get; set; }

        public string account_code { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int total { get; set; }

        public short status { get; set; }

        public string created_at { get; set; }
    }
}