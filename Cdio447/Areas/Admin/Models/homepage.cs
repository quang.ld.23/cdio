﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cdio447.Areas.Admin.Models
{
    public class homepage
    {
        public decimal totalamount { get; set; }

        public int totaluserOnline { get; set; }

        public int withdraw { get; set; }

        public int totaltransactionToday { get; set; }
    }
}