﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cdio447.Areas.Admin.Models
{
    public class Gender
    {
        public short? ID { get; set; }
        public string Name { get; set; }
    }
}