﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cdio447.Areas.Admin.Models
{
    public class payinto_histories_View
    {
        public string user_name { get; set; }

        public string bank { get; set; }

        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int amount { get; set; }

        public string oder_code { get; set; }

        public DateTime created_at { get; set; }
    }
}