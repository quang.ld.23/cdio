﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cdio447.Areas.Admin.Models
{
    public class douHistoriesView
    {
        [Display(Name ="Người được thuê")]
        public string user_name_dou { get; set; }

        [Display(Name = "Người thuê")]
        public string user_name_rent { get; set; }

        [Display(Name = "Số lần thuê")]
        public int total_time { get; set; }

        [Display(Name = "Tổng tiền")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int total_amount { get; set; }

        [Display(Name = "Ngày cho thuê")]
        public DateTime created_at { get; set; }
    }
}