﻿using Cdio447.Areas.Admin.Models.Infrastructure;
using Cdio447.Models;
using System.Collections.Generic;
using System.Linq;

namespace Cdio447.Areas.Admin.Models

{
    public class Historiesdou : IHistoriesdou
    {
        private DBContext ctx = null;

        public Historiesdou()
        {
            ctx = new DBContext();
        }

        public IEnumerable<douHistoriesView> lisDou()
        {
            var list = (from a in ctx.dou_histories
                        join b in ctx.users
                        on a.user_id_dou equals b.id
                        join c in ctx.users
                        on a.user_id_rent equals c.id
                        select new
                        {
                            name_dou = b.user_name,
                            name_rent = c.user_name,
                            total_time = a.total_time,
                            total_amount = a.total_amount,
                            created_at = a.created_at
                        }).Select(u => new douHistoriesView()
                        {
                            user_name_dou = u.name_dou,
                            user_name_rent = u.name_rent,
                            total_time = u.total_time,
                            total_amount = (int)u.total_amount,
                            created_at = u.created_at
                        });
            return list;
        }
    }
}