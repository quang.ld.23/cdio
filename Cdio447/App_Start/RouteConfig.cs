﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Cdio447
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Cdio447.Controllers" }
            );
            routes.MapRoute("User Detail", "User/Detail/{id}", new
            {
                controller = "User",
                action = "Detail",
                id = UrlParameter.Optional
            });
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // BotDetect requests must not be routed 
            routes.IgnoreRoute("{*botdetect}",
              new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });
        }
    }
}