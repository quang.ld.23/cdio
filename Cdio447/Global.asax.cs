using Cdio447.Models;
using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Cdio447.Code;
namespace Cdio447
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            DefaultModelBinder.ResourceClassKey = "ResourceError";
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer<DBContext>(new DropCreateDatabaseIfModelChanges<DBContext>());
        }
    }
}