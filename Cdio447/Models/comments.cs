namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class comments
    {
        public int id { get; set; }

        public int user_id_cmt { get; set; }

        public int user_id_rcmt { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string contents { get; set; }

        public short number_star { get; set; }

        public DateTime created_at { get; set; }

        public virtual users user_cmt { get; set; }

        public virtual users user_rcmt { get; set; }
    }
}
