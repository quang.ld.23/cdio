namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class rent_transaction
    {
        public int id { get; set; }

        public int user_id_dou { get; set; }

        public int user_id_rent { get; set; }

        public short total_time { get; set; }

        [Column(TypeName = "ntext")]
        public string message { get; set; }

        public DateTime created_at { get; set; }

        public short status { get; set; }

        public virtual users user_dou { get; set; }

        public virtual users user_rent { get; set; }
    }
}
