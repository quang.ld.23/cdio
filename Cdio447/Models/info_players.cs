namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class info_players
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int user_id { get; set; }

        [StringLength(50)]
        public string title { get; set; }

        [Column(TypeName = "ntext")]
        public string description { get; set; }

        [StringLength(100)]
        public string link_fb { get; set; }

        [StringLength(100)]
        public string link_insta { get; set; }

        [StringLength(100)]
        public string link_messenger { get; set; }

        [StringLength(100)]
        public string link_youtube { get; set; }

        public int? total_time { get; set; }

        public decimal? amount_in_one { get; set; }

        [StringLength(100)]
        public string tag { get; set; }

        public virtual users users { get; set; }
    }
}
