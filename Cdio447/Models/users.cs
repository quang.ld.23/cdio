namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public users()
        {
            comment_cmt = new HashSet<comments>();
            comment_rcmt = new HashSet<comments>();
            dou_histories_dou = new HashSet<dou_histories>();
            dou_histories_rent = new HashSet<dou_histories>();
            payinto_histories = new HashSet<payinto_histories>();
            player_game = new HashSet<player_game>();
            rent_transaction_user_dou = new HashSet<rent_transaction>();
            rent_transaction_user_rent = new HashSet<rent_transaction>();
            withdrawl_histories = new HashSet<withdrawl_histories>();
        }

        public int id { get; set; }


        [Required]
        [StringLength(30)]
        public string user_name { get; set; }


        [Required]
        [StringLength(100)]
        public string password { get; set; }

        [Required]
        [StringLength(100)]
        public string display_name { get; set; }

        [Required]
        [StringLength(50)]
        public string phone { get; set; }

        [Required]
        [StringLength(50)]
        public string email { get; set; }

        public short role { get; set; }

        public short? gender { get; set; }

        public int city_id { get; set; }

        [StringLength(100)]
        public string avatar { get; set; }

        public decimal? current_money { get; set; }

        public short? status { get; set; }

        public int? total_time_rent { get; set; }

        public virtual cities cities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<comments> comment_cmt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<comments> comment_rcmt { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dou_histories> dou_histories_dou { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dou_histories> dou_histories_rent { get; set; }

        public virtual info_players info_players { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<payinto_histories> payinto_histories { get; set; }

        public virtual payment_gateways payment_gateways { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<player_game> player_game { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rent_transaction> rent_transaction_user_dou { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rent_transaction> rent_transaction_user_rent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<withdrawl_histories> withdrawl_histories { get; set; }
    }
}
