namespace Cdio447.Models
{
    using System;

    public partial class dou_histories
    {
        public int id { get; set; }

        public int user_id_dou { get; set; }

        public int user_id_rent { get; set; }

        public int total_time { get; set; }

        public decimal total_amount { get; set; }

        public DateTime created_at { get; set; }

        public virtual users user_dou { get; set; }
        public virtual users user_rent { get; set; }
    }
}