namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class payinto_histories
    {
        public int id { get; set; }

        public int user_id { get; set; }

        public int bank_id { get; set; }

        public decimal amount { get; set; }

        [Required]
        [StringLength(50)]
        public string oder_code { get; set; }

        public DateTime created_at { get; set; }

        public virtual banks banks { get; set; }

        public virtual users users { get; set; }
    }
}
