namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class payment_gateways
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int user_id { get; set; }

        public int bank_id { get; set; }

        public string account_name { get; set; }

        public string account_code { get; set; }

        public virtual banks banks { get; set; }

        public virtual users users { get; set; }
    }
}
