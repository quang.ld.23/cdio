namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class player_game
    {
        public int id { get; set; }

        public int game_id { get; set; }

        public int user_id { get; set; }

        public virtual games games { get; set; }

        public virtual users users { get; set; }
    }
}
