namespace Cdio447.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.Validation;

    public partial class DBContext : DbContext
    {
        public DBContext()
            : base("name=DBContext")
        {
        }

        public virtual DbSet<banks> banks { get; set; }
        public virtual DbSet<cities> cities { get; set; }
        public virtual DbSet<comments> comments { get; set; }
        public virtual DbSet<dou_histories> dou_histories { get; set; }
        public virtual DbSet<games> games { get; set; }
        public virtual DbSet<info_players> info_players { get; set; }
        public virtual DbSet<payinto_histories> payinto_histories { get; set; }
        public virtual DbSet<payment_gateways> payment_gateways { get; set; }
        public virtual DbSet<player_game> player_game { get; set; }
        public virtual DbSet<rent_transaction> rent_transaction { get; set; }
        public virtual DbSet<users> users { get; set; }
        public virtual DbSet<withdrawl_histories> withdrawl_histories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<banks>()
                .HasMany(e => e.payinto_histories)
                .WithRequired(e => e.banks)
                .HasForeignKey(e => e.bank_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<banks>()
                .HasMany(e => e.payment_gateways)
                .WithRequired(e => e.banks)
                .HasForeignKey(e => e.bank_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cities>()
                .HasMany(e => e.users)
                .WithRequired(e => e.cities)
                .HasForeignKey(e => e.city_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<dou_histories>()
                .Property(e => e.total_amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<games>()
                .HasMany(e => e.player_game)
                .WithRequired(e => e.games)
                .HasForeignKey(e => e.game_id);

            modelBuilder.Entity<info_players>()
                .Property(e => e.amount_in_one)
                .HasPrecision(18, 0);

            modelBuilder.Entity<users>()
                .Property(e => e.current_money)
                .HasPrecision(18, 0);

            modelBuilder.Entity<users>()
                .HasMany(e => e.comment_cmt)
                .WithRequired(e => e.user_cmt)
                .HasForeignKey(e => e.user_id_cmt)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.comment_rcmt)
                .WithRequired(e => e.user_rcmt)
                .HasForeignKey(e => e.user_id_rcmt);

            modelBuilder.Entity<users>()
                .HasMany(e => e.dou_histories_dou)
                .WithRequired(e => e.user_dou)
                .HasForeignKey(e => e.user_id_dou);

            modelBuilder.Entity<users>()
                .HasMany(e => e.dou_histories_rent)
                .WithRequired(e => e.user_rent)
                .HasForeignKey(e => e.user_id_rent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasOptional(e => e.info_players)
                .WithRequired(e => e.users);

            modelBuilder.Entity<users>()
                .HasMany(e => e.payinto_histories)
                .WithRequired(e => e.users)
                .HasForeignKey(e => e.user_id);

            modelBuilder.Entity<users>()
                .HasOptional(e => e.payment_gateways)
                .WithRequired(e => e.users);

            modelBuilder.Entity<users>()
                .HasMany(e => e.player_game)
                .WithRequired(e => e.users)
                .HasForeignKey(e => e.user_id);

            modelBuilder.Entity<users>()
                .HasMany(e => e.rent_transaction_user_dou)
                .WithRequired(e => e.user_dou)
                .HasForeignKey(e => e.user_id_dou)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.rent_transaction_user_rent)
                .WithRequired(e => e.user_rent)
                .HasForeignKey(e => e.user_id_rent)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<users>()
                .HasMany(e => e.withdrawl_histories)
                .WithRequired(e => e.users)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<withdrawl_histories>()
                .Property(e => e.total)
                .HasPrecision(18, 0);
        }
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
    }
}
