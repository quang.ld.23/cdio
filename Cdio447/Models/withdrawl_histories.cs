namespace Cdio447.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class withdrawl_histories
    {
        public int id { get; set; }

        public int user_id { get; set; }

        public decimal total { get; set; }

        public short status { get; set; }

        public DateTime created_at { get; set; }

        public virtual users users { get; set; }
    }
}
