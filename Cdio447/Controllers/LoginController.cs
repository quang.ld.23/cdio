using Cdio447.Code;
using Cdio447.Models;
using Cdio447.ViewModels;
using System.Web.Mvc;

namespace Cdio447.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var dao = new AccountModel();
                var result = dao.Login(model.UserName, model.PassWord);
                if (result == 1)// user là admin
                {
                    ViewBag.Message = "Your account does not have access";
                }
                else if (result == 2)// là user
                {
                    var user = dao.GetById(model.UserName);
                    var userSession = new UserSession();
                    userSession.User_id = user.id;
                    userSession.UserName = user.user_name;
                    userSession.Role = user.role;
                    userSession.email = user.email;
                    userSession.user_name = user.user_name;
                    userSession.phone = user.phone;
                    userSession.current_money = (decimal)user.current_money;
                    userSession.status = (short)user.status;
                    // ep session ve object tranh lỗi
                    Session.Add(CommonConstants.USER_SESSION, userSession);
                    return RedirectToAction("Index", "Home");
                }
                else if (result == 0)
                {
                    ModelState.AddModelError("", "Sai tên đăng nhập hoặc mật khẩu.");
                }
                else if (result == -1)
                {
                    ModelState.AddModelError("", "Vui lòng nhập Tên đăng nhập và Mật khẩu.");
                }
            }
            else
            {
                ModelState.AddModelError("", "Dữ liệu nhập không hợp lệ");
            }
            return View();
        }

        // logout
        public ActionResult Logout()
        {
            Session[CommonConstants.USER_SESSION] = null;

            return RedirectToAction("Index", "Home");
        }
    }
}