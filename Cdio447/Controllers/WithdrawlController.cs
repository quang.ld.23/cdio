﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cdio447.Models;
using Cdio447.Code;
using Cdio447.ViewModels;

namespace Cdio447.Controllers
{
    public class WithdrawlController : Controller
    {
        private DBContext db = new DBContext();
        // GET: Withdrawl
        public ActionResult PaymentGateway()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var banks = db.banks.ToList();
                ViewBag.banks = banks;
                ViewBag.success = null;
                ViewBag.CheckModel = "";
                var model = db.payment_gateways.Find(session.User_id);
                if(model == null)
                {
                    ViewBag.CheckModel = null;
                }
                return View(model);
            }
            return RedirectToAction("Login", "Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PaymentGateway(payment_gateways pw)
        {
            var banks = db.banks.ToList();
            ViewBag.banks = banks;
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                pw.user_id = session.User_id;
                if (ModelState.IsValid)
                {
                    if(pw.bank_id == 0)
                    {
                        ModelState.AddModelError("bank_id", "Vui lòng chọn cổng thanh toán");
                    } else
                    {
                        payment_gateways getPW = db.payment_gateways.SingleOrDefault(x => x.user_id == session.User_id);
                        if (getPW == null)
                        {
                            db.payment_gateways.Add(pw);
                            db.SaveChanges();
                            ViewBag.success = "";
                        }
                        else
                        {
                            getPW.bank_id = pw.bank_id;
                            getPW.account_code = pw.account_code;
                            getPW.account_name = pw.account_name;
                            ViewBag.success = "";
                            db.SaveChanges();
                        }
                    }
                }
                return View(pw);
            }
            return RedirectToAction("Login", "Login");
        }
        [HttpGet]
        public ActionResult Withdrawl()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var pwModel = db.payment_gateways.SingleOrDefault(x => x.user_id == session.User_id);
                ViewBag.pwModel = pwModel;
                var getWH = db.withdrawl_histories.Where(x => x.user_id == session.User_id).ToList();
                ViewBag.CheckModel = "";
                ViewBag.CheckHistory = null;
                ViewBag.amountAble = getAmountAble();
                if (pwModel == null)
                    ViewBag.CheckModel = null;
                if (getWH != null)
                    ViewBag.CheckHistory = getWH;
                return View();
            }
            return RedirectToAction("Login", "Login");
        }

        [HttpPost]
        public ActionResult Withdrawl(WidthdrawlViewModel wd)
        {
            
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var pwModel = db.payment_gateways.SingleOrDefault(x => x.user_id == session.User_id);
                ViewBag.pwModel = pwModel;
                var getWH = db.withdrawl_histories.Where(x => x.user_id == session.User_id).ToList();
                ViewBag.CheckModel = "";
                ViewBag.CheckHistory = null;
                ViewBag.amountAble = getAmountAble();
                decimal currentMoney = (decimal)pwModel.users.current_money;
                if (pwModel == null)
                    ViewBag.CheckModel = null;
                if (getWH != null)
                    ViewBag.CheckHistory = getWH;
                if (ModelState.IsValid)
                {
                    decimal convert = decimal.Parse(wd.amount);
                    if ((convert % 50000) != 0)
                    {
                        ModelState.AddModelError("amount", "Số tiền rút phải là bội số của 50,000");
                    } else if(convert <= 250000)
                    {
                        ModelState.AddModelError("amount", "Số tiền rút phải lớn hơn hoặc bằng 250,000 VND");
                    } else if (convert > currentMoney)
                    {
                        ModelState.AddModelError("amount", "Số tiền rút phải nhỏ hơn số tiền hiện có");
                    } else
                    {
                        withdrawl_histories newWd = new withdrawl_histories();
                        string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                        DateTime date = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm:ss", null);
                        newWd.user_id = session.User_id;
                        newWd.total = convert;
                        newWd.status = 1;//dang xu ly
                        newWd.created_at = date;
                        db.withdrawl_histories.Add(newWd);
                        users user = db.users.Find(session.User_id);
                        user.current_money = user.current_money - convert;
                        db.SaveChanges();
                        ViewBag.success = "";
                    }
                }
                return View(wd);
            }
            return RedirectToAction("Login", "Login");
        }

        public decimal getAmountAble()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            var pwModel = db.users.SingleOrDefault(x => x.id == session.User_id);
            decimal currentMoney = (decimal)pwModel.current_money;
            decimal amountAble = 0;
            if (currentMoney >= 250000)
            {
                amountAble = currentMoney - 50000;
            }
            return amountAble;
        }
    }
}