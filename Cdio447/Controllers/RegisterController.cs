﻿using Cdio447.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cdio447.ViewModels;
using Cdio447.Models;
using Cdio447.Supports;
using System.Net;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace Cdio447.Controllers
{
    public class RegisterController : Controller
    { private AccountModel res = new AccountModel();
        // GET: Register
        [HttpGet]
        
        public ActionResult Index()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];

            if (session != null)
            {
                int role = session.Role;
                if (role == 2)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                { // là admin thì đăng xuất ra
                    Session[CommonConstants.USER_SESSION] = null;
                    return View();

                }
                
            }

            SetViewbag();
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Index(RegisterModels model)
        {
            var register = new AccountModel();
            if (ModelState.IsValid)
            {
                
                if (register.Check_User_name(model.user_name))
                {
                    ModelState.AddModelError("", "Tên đăng nhập đã tồn tại");
                }
                else if (register.Check_Email(model.email))
                {
                    ModelState.AddModelError("", "Email đã được đăng kí ");
                }
                else
                { if (IsValidRecaptcha(Request["g-recaptcha-response"]))
                    {
                        var user = new users();

                        user.user_name = model.user_name;
                        user.password = Encryptor_MD5.MD5Hash(model.password);
                        user.email = model.email;
                        user.phone = model.phone;
                        user.display_name = model.display_name;
                        user.gender = model.gender;
                        user.city_id = model.city_id;
                        user.current_money = 0;
                        user.role = 2;
                        user.avatar = "avatar_default.jpg";
                        var result = register.Insert(user);
                        if (result > 0)
                        {
                            //model = new RegisterModels();
                            //var login = new LoginModel();
                            //login.UserName = model.user_name;
                            //login.PassWord = model.password;
                            //return RedirectToAction("Login", "Login", login);
                            ViewBag.succes = "Đăng kí thành công!";
                           

                        }
                        else ViewBag.fail = "Đăng kí không thành công";
                    }
                    else
                        ViewBag.capcha = "Vui lòng xác thực capcha";
                }
               

            }
            else
            {
                ViewBag.Mess = "Dữ liệu nhập vào không hợp lệ -Vui lòng thử lại!";
            }
           
            SetViewbag();
            return View(model);

        }
        public void SetViewbag()
        {
            //DropList City
            ViewBag.CityID = new SelectList(res.listciy(), "id", "name");

            // Droplist Gender
            List<SelectListItem> sex_items = new List<SelectListItem>();
            sex_items.Add(new SelectListItem() { Text = "Nam", Value = "1" });
            sex_items.Add(new SelectListItem() { Text = "Nữ", Value = "2" });
            ViewBag.Gender = sex_items;


        }
        //Recaptcha for google
        private bool IsValidRecaptcha(string recaptcha)
        {
            if (string.IsNullOrEmpty(recaptcha))
            {
                return false;
            }
            var secretKey = "6LfftLYUAAAAAMlWP6qUE6NcMJtFl0qrSARFbl-F";
            string remoteIp = Request.ServerVariables["REMOTE_ADDR"];
            string myParameters = String.Format("secret={0}&response={1}&remoteip={2}", secretKey, recaptcha, remoteIp);
            RecaptchaResult captchaResult;
            using (var wc = new WebClient())
            {
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                var json = wc.UploadString("https://google.com/recaptcha/api/siteverify", myParameters);
                var js = new DataContractJsonSerializer(typeof(RecaptchaResult));
                var ms = new MemoryStream(Encoding.ASCII.GetBytes(json));
                captchaResult = js.ReadObject(ms) as RecaptchaResult;
                if (captchaResult != null && captchaResult.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }
    }
}