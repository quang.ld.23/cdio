using Cdio447.Code;
using Cdio447.Models;
using Cdio447.ViewModels;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Cdio447.Controllers
{
    public class HomeController : Controller
    {
        public DBContext db = new DBContext();
        private UserSupport user_sp = new UserSupport();

        public ActionResult Index()
        {
            var model = user_sp.ListUserHP();
            var city = db.cities.ToList();
            ViewBag.city = city;
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult Avatar_User()
        {
            var sesion = (UserSession)Session[CommonConstants.USER_SESSION];

            if (sesion != null)
            {
                int role = sesion.Role;
                if (role != 1)
                {
                    var avatar = new AccountModel().AvatarUser(sesion.UserName);
                    return PartialView(avatar);
                }
                else
                {
                    string name = "e10adc3949ba59abbe56e057f20f883e";
                    var avatar = new AccountModel().AvatarUser(name);
                    return PartialView(avatar);
                }
            }
            else
            {
                string name = "e10adc3949ba59abbe56e057f20f883e";
                var avatar = new AccountModel().AvatarUser(name);
                return PartialView(avatar);
            }
        }
        //hiện thị form đánh giá
        [ChildActionOnly]
        public ActionResult Comment_player()
        {
            var sesion = (UserSession)Session[CommonConstants.USER_SESSION];

            if (sesion != null)
            {
                var avatar = new AccountModel().AvatarUser(sesion.UserName);
                return PartialView(avatar);
            }
            else
            {
                string name = "e10adc3949ba59abbe56e057f20f883e";
                var avatar = new AccountModel().AvatarUser(name);
                return PartialView(avatar);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Filter(AjaxFilter jsonFilter)
        {
            var new_min_price = Convert.ToDecimal(jsonFilter.min_price);
            var new_max_price = Convert.ToDecimal(jsonFilter.max_price);
            var models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id
                          && b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price
                          select new
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              status = a.status,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag
                          });
            if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] == "null")
            {
                short new_gender = short.Parse(jsonFilter.gender);
                models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id
                          where a.status == 1 //online
                          where (b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && a.gender == new_gender)
                          select new
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              status = a.status,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag
                          });
            }
            else if (jsonFilter.gender == "null" && jsonFilter.arr_cities[0] != "null")
            {
                int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id
                          where a.status == 1 //online
                          where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && new_arr_cities.Contains(a.city_id)
                          select new
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              status = a.status,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag
                          });
            }
            else if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] != "null")
            {
                short new_gender = short.Parse(jsonFilter.gender);
                int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id
                          where a.status == 1 //online
                          where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price
                          && new_arr_cities.Contains(a.city_id) && a.gender == new_gender
                          select new
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              status = a.status,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag
                          });
            }
            if (jsonFilter.name != "null" && jsonFilter.tag == "null")
            {
                models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id
                          && (b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price)
                          && a.display_name.Contains(jsonFilter.name)
                          select new
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              status = a.status,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag
                          });
                if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] == "null")
                {
                    short new_gender = short.Parse(jsonFilter.gender);
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where (b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && a.gender == new_gender)
                              && a.display_name.Contains(jsonFilter.name)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
                else if (jsonFilter.gender == "null" && jsonFilter.arr_cities[0] != "null")
                {
                    int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && new_arr_cities.Contains(a.city_id)
                              && a.display_name.Contains(jsonFilter.name)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
                else if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] != "null")
                {
                    short new_gender = short.Parse(jsonFilter.gender);
                    int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price
                              && new_arr_cities.Contains(a.city_id) && a.gender == new_gender
                              && a.display_name.Contains(jsonFilter.name)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
            }
            else if (jsonFilter.name == "null" && jsonFilter.tag != "null")
            {
                models = (from a in db.users
                          join b in db.info_players
                          on a.id equals b.user_id
                          where a.id == b.user_id
                          && (b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price)
                          && b.tag.Contains(jsonFilter.tag)
                          select new
                          {
                              user_id = b.user_id,
                              display_name = a.display_name,
                              avatar = a.avatar,
                              status = a.status,
                              amount_in_one = b.amount_in_one,
                              tag = b.tag
                          });
                if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] == "null")
                {
                    short new_gender = short.Parse(jsonFilter.gender);
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where (b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && a.gender == new_gender)
                              && b.tag.Contains(jsonFilter.tag)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
                else if (jsonFilter.gender == "null" && jsonFilter.arr_cities[0] != "null")
                {
                    int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && new_arr_cities.Contains(a.city_id)
                              && b.tag.Contains(jsonFilter.tag)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
                else if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] != "null")
                {
                    short new_gender = short.Parse(jsonFilter.gender);
                    int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price
                              && new_arr_cities.Contains(a.city_id) && a.gender == new_gender
                              && b.tag.Contains(jsonFilter.tag)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
            }
            else if (jsonFilter.name != "null" && jsonFilter.tag != "null")
            {
                if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] == "null")
                {
                    short new_gender = short.Parse(jsonFilter.gender);
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where (b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && a.gender == new_gender)
                              && a.display_name.Contains(jsonFilter.name)
                              && b.tag.Contains(jsonFilter.tag)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
                else if (jsonFilter.gender == "null" && jsonFilter.arr_cities[0] != "null")
                {
                    int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price && new_arr_cities.Contains(a.city_id)
                              && a.display_name.Contains(jsonFilter.name)
                              && b.tag.Contains(jsonFilter.tag)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
                else if (jsonFilter.gender != "null" && jsonFilter.arr_cities[0] != "null")
                {
                    short new_gender = short.Parse(jsonFilter.gender);
                    int[] new_arr_cities = Array.ConvertAll(jsonFilter.arr_cities, s => int.Parse(s));
                    models = (from a in db.users
                              join b in db.info_players
                              on a.id equals b.user_id
                              where a.id == b.user_id
                              where a.status == 1 //online
                              where b.amount_in_one >= new_min_price && b.amount_in_one <= new_max_price
                              && new_arr_cities.Contains(a.city_id) && a.gender == new_gender
                              && a.display_name.Contains(jsonFilter.name)
                              && b.tag.Contains(jsonFilter.tag)
                              select new
                              {
                                  user_id = b.user_id,
                                  display_name = a.display_name,
                                  avatar = a.avatar,
                                  status = a.status,
                                  amount_in_one = b.amount_in_one,
                                  tag = b.tag
                              });
                }
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            var convertJson = JsonConvert.SerializeObject(models);
            return Json(models);
        }
    }
}