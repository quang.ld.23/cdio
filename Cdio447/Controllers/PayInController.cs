﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cdio447.Models;
using Cdio447.Model_Supports;
using Cdio447.Code;
using Cdio447.Supports;

namespace Cdio447.Controllers
{
    public class PayInController : Controller
    {
        private DBContext db = new DBContext();
        // GET: PayIn
        private RequestCheckOrder requestCheck = new RequestCheckOrder();
        private string merchantId = "47902";
        private string merchantPassword = "6b6008e008292b18f94df4fae5c93017";
        public ActionResult Index()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var banks = db.banks.ToList();
                ViewBag.bank = banks;
                ViewBag.CheckHistoy = null;
                var getHistories = db.payinto_histories.Where(x => x.user_id == session.User_id).ToList();
                if (getHistories != null)
                    ViewBag.CheckHistoy = getHistories;
                return View();
            }
            return RedirectToAction("Login", "Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PayInSupport pi)
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            var banks = db.banks.ToList();
            ViewBag.bank = banks;
            ViewBag.CheckHistoy = null;
            var getHistories = db.payinto_histories.Where(x => x.user_id == session.User_id).ToList();
            if (getHistories != null)
                ViewBag.CheckHistoy = getHistories;
            if (ModelState.IsValid)
            {
                decimal getAmount = decimal.Parse(pi.amount);
                if(getAmount < 200000 || getAmount > 10000000)
                {
                    ModelState.AddModelError("amount", "Số tiền nạp từ 200,000 đến 10,000,000 VND");
                } else if((getAmount % 50000) != 0)
                {
                    ModelState.AddModelError("amount", "Số tiền nạp phải là bội số của 50,000");
                }
                else {
                    var payInSession = new PayInSession();
                    string payment_method = "ATM_ONLINE";
                    string str_bankcode = pi.banks;

                    decimal getFee = decimal.Parse(pi.amount);
                    getFee = getFee - 5000;
                    string Total_amount = getFee.ToString();

                    payInSession.amount = Total_amount;
                    payInSession.bank_id = pi.bank_id;
                    Session.Add(CommonConstants.PayInSession, payInSession);

                    RequestInfo info = new RequestInfo();
                    info.Merchant_id = "47902";
                    info.Merchant_password = "6b6008e008292b18f94df4fae5c93017";
                    info.Receiver_email = "duongmanucian2906@gmail.com";
                    info.cur_code = "vnd";
                    info.bank_code = str_bankcode;
                    info.Order_code = session.user_name + "-" + FunctionSupport.RandomString(5);

                    info.Total_amount = Convert.ToString(Total_amount);
                    info.fee_shipping = "0";
                    info.Discount_amount = "0";
                    info.order_description = "Thanh toán thành công";
                    info.return_url = "http://localhost:51465/PayIn/Success";
                    info.cancel_url = "http://localhost:51465/PayIn/Error";

                    info.Buyer_fullname = session.user_name;
                    info.Buyer_email = session.email;
                    info.Buyer_mobile = session.phone;

                    APICheckoutV3 objNLCheckout = new APICheckoutV3();
                    ResponseInfo result = objNLCheckout.GetUrlCheckout(info, payment_method);

                    if (result.Error_code == "00")
                    {
                        return Redirect(result.Checkout_url);
                    }
                    return Redirect("http://localhost:51465/PayIn/Error");
                }
            }
            return View(pi);
        }
        public ActionResult Success()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            var getToken = Request.QueryString["token"];
            var getErrorCode = Request.QueryString["error_code"];
            if (getToken == null || session == null || getErrorCode == null)
            {
                return RedirectToAction("Login", "Login");
            }
            requestCheck.Merchant_id = this.merchantId;
            requestCheck.Merchant_password = this.merchantPassword;
            requestCheck.Token = getToken;
            APICheckoutV3 objNLCheckout = new APICheckoutV3();
            ResponseCheckOrder objResult = objNLCheckout.GetTransactionDetail(requestCheck);
            var getCode = objResult.errorCode;
            var getOrderCode = Request.QueryString["order_code"];
            var payInSession = (PayInSession)Session[CommonConstants.PayInSession];

            if (getCode == "00" && getErrorCode == "00")
            {
                decimal new_amount = decimal.Parse(payInSession.amount);
                using (DBContext db = new DBContext())
                {
                    payinto_histories ph = new payinto_histories();
                    ph.user_id = session.User_id;
                    ph.bank_id = payInSession.bank_id;
                    ph.amount = decimal.Parse(payInSession.amount);
                    ph.oder_code = getOrderCode;
                    string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    DateTime date = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm:ss", null);
                    ph.created_at = date;
                    db.payinto_histories.Add(ph);
                    db.SaveChanges();

                    users user = db.users.SingleOrDefault(x => x.id == session.User_id);
                    user.current_money = user.current_money + new_amount;
                    db.SaveChanges();
                }
            }
            ViewBag.getAmount = payInSession.amount;
            Session[CommonConstants.PayInSession] = null;
            payInSession = null;
            return View();
        }
        public ActionResult Error()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            var getToken = Request.QueryString["token"];
            var getErrorCode = Request.QueryString["error_code"];
            if (getToken == null || session == null || getErrorCode == null)
            {
                return View();
            }
            return RedirectToAction("Index", "Home");
        }
    }
}