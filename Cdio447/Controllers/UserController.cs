﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cdio447.ViewModels;
using Cdio447.Models;
using Cdio447.Code;
using System.Data.Entity;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Cdio447.Supports;


namespace Cdio447.Controllers
{
    public class UserController : Controller
    {
        private AccountModel res = new AccountModel();
        public UserSupport user_sp = new UserSupport();
        public DBContext db = new DBContext();

        public ActionResult Detail(int id)
        {
            var model = db.info_players.Find(id);
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            ViewBag.checkLogin = session;
            ViewBag.checkRent = 0;
            if (session != null)
            {
                rent_transaction checkRent = db.rent_transaction.OrderByDescending(x => x.created_at)
                .Where(x => x.user_id_dou == id && x.user_id_rent == session.User_id).FirstOrDefault();
                if (checkRent != null)
                {
                    DateTime now = DateTime.Now;
                    DateTime getCreatedAt = checkRent.created_at;
                    TimeSpan getDistance = now - getCreatedAt;
                    if (getDistance.Days <= 0 && getDistance.Hours <= 0 && getDistance.Minutes <= 5)
                    {
                        ViewBag.checkRent = 1;
                    }
                }
            }
            if (model == null)
                return View("~/Views/NotFound-404.cshtml");
            return View(model);
        }
        [HttpPost]
        public JsonResult Comment(AjaxComment ajaxData)
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            var check = db.dou_histories
                    .Where(x => x.user_id_dou == ajaxData.userIdRcmt)
                    .Where(x => x.user_id_rent == session.User_id).FirstOrDefault();
            if (session.User_id == ajaxData.userIdRcmt)
                return Json(1);
            else if (check == null)
                return Json(2);
            else if (ajaxData.getNumberStar == "" || ajaxData.getContent == "" || ajaxData.getContent == null || ajaxData.getNumberStar == null)
                return Json(3);
            else if (ajaxData.getContent.Length < 15)
                return Json(4);
            string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            DateTime date = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm:ss", null);
            comments cmt = new comments();
            cmt.user_id_cmt = session.User_id;
            cmt.user_id_rcmt = ajaxData.userIdRcmt;
            cmt.contents = ajaxData.getContent;
            cmt.number_star = short.Parse(ajaxData.getNumberStar);
            cmt.created_at = date;
            db.comments.Add(cmt);
            db.SaveChanges();
            return Json(5);
        }
        [HttpPost]
        public JsonResult AjaxPagingComment(int userId, int pageIndex)
        {
            PageComments model = new PageComments();
            model.PageIndex = pageIndex;
            model.PageSize = 5;
            model.RecordCount = db.comments.Where(x => x.user_id_rcmt == userId).ToList().Count;
            if (model.RecordCount <= 0)
            {
                return Json(1);
            }
            int startIndex = (pageIndex - 1) * model.PageSize;
            var getListComments = (from a in db.comments
                                   select new
                                   {
                                       id = a.id,
                                       user_name_cmt = a.user_cmt.display_name,
                                       avatar = a.user_cmt.avatar,
                                       user_id_rcmt = a.user_id_rcmt,
                                       contents = a.contents,
                                       number_star = a.number_star,
                                       created_at = a.created_at,
                                   })
                            .Where(x => x.user_id_rcmt == userId)
                            .OrderByDescending(x => x.created_at)
                            .Skip(startIndex)
                            .Take(model.PageSize).ToList();
            model.getListComments = getListComments;
            JavaScriptSerializer js = new JavaScriptSerializer();
            var convertJson = JsonConvert.SerializeObject(model);
            return Json(convertJson);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AjaxRentPlayer(int userIdDou, string message, int totalTime)
        {
            try
            {
                var session = (UserSession)Session[CommonConstants.USER_SESSION];
                if (userIdDou == session.User_id)
                    return Json(0);

                info_players getUserDou = db.info_players.FirstOrDefault(x => x.user_id == userIdDou);
                users getUserRent = db.users.FirstOrDefault(x => x.id == session.User_id);

                decimal getAmountUser = (decimal)getUserDou.amount_in_one * totalTime;
                if (getUserRent.current_money < getAmountUser)
                {
                    return Json(2);
                }

                rent_transaction getRent = new rent_transaction();
                getRent.user_id_rent = session.User_id;
                getRent.user_id_dou = userIdDou;
                string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                DateTime date = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm:ss", null);
                getRent.created_at = date;
                getRent.status = 1;//waiting player
                getRent.total_time = (short)totalTime;//waiting player
                db.rent_transaction.Add(getRent);
                db.SaveChanges();
                return Json(1);//success
            }
            catch (Exception ex)
            {
                return Json(-1);//error
            }
        }
        public ActionResult NotificationList()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var getTransaction = db.rent_transaction.OrderByDescending(x => x.created_at)
                    .Where(x => x.user_id_dou == session.User_id).ToList();
                ViewBag.getTransaction = getTransaction == null ? null : getTransaction;
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }
        //get time rent
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult getTimeRent(int idUser)
        {
            users getUser = db.users.FirstOrDefault(x => x.id == idUser);
            if (getUser.status == 2)
            {
                rent_transaction checkTimeRent = db.rent_transaction.OrderByDescending(x => x.created_at)
                .Where(x => x.user_id_dou == idUser && x.status == 3).FirstOrDefault();
                if (checkTimeRent != null)
                {
                    var getHourCreated = checkTimeRent.total_time;
                    DateTime now = DateTime.Now;
                    DateTime getCreatedAt = checkTimeRent.created_at;
                    DateTime addHour = getCreatedAt.AddHours(getHourCreated);
                    TimeSpan getDistance = now - getCreatedAt;
                    TimeSpan getDistanceAddHour = now - addHour;
                    if (getDistance.Days >= 1 || getDistance.Hours >= getHourCreated)
                    {
                        getUser.status = 1;//change to ready
                        db.SaveChanges();
                        return Json(1);//change success
                    }
                    else
                    {
                        List<int> arrTime = new List<int>();
                        arrTime.Add(getDistanceAddHour.Hours * -1);
                        arrTime.Add(getDistanceAddHour.Minutes * -1);
                        arrTime.Add(getDistanceAddHour.Seconds * -1);
                        return Json(arrTime);
                    }
                }
            }
            return Json(0);//not handle
        }

        //accept request 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Accept(int userIdDou, int userIdRent, DateTime createdAt)
        {
            try
            {
                rent_transaction checkTimeRent = db.rent_transaction.OrderByDescending(x => x.created_at)
                    .Where(x => x.user_id_dou == userIdDou && x.user_id_rent == userIdRent).FirstOrDefault();
                if (checkTimeRent != null)
                {
                    DateTime now = DateTime.Now;
                    DateTime getCreatedAt = checkTimeRent.created_at;
                    TimeSpan getDistance = now - getCreatedAt;
                    if (getDistance.Days >= 1 || getDistance.Hours >= 1 || getDistance.Minutes > 5)
                        return Json(0);//timeout
                }
                rent_transaction checkRent = db.rent_transaction.OrderByDescending(x => x.created_at).Where(x => x.user_id_dou.Equals(userIdDou))
                    .Where(x => x.user_id_rent.Equals(userIdRent))
                    .Where(x => DbFunctions.TruncateTime(x.created_at) == DbFunctions.TruncateTime(createdAt)).FirstOrDefault();
                if (checkRent == null)
                    return Json(-1);//error
                else
                {
                    users getUserDou = db.users.FirstOrDefault(x => x.id == userIdDou);
                    users getUserRent = db.users.FirstOrDefault(x => x.id == userIdRent);
                    getUserDou.status = 2;//
                    checkRent.status = 3;//change status to reject
                    //add dou_historiy
                    dou_histories dh = new dou_histories();
                    dh.user_id_dou = userIdDou;
                    dh.user_id_rent = userIdRent;
                    dh.total_time = checkRent.total_time;
                    dh.total_amount = dh.total_time * (decimal)getUserDou.info_players.amount_in_one;
                    string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    DateTime date = DateTime.ParseExact(datetime, "dd/MM/yyyy HH:mm:ss", null);
                    dh.created_at = date;
                    db.dou_histories.Add(dh);
                    //subtract amount user rent
                    getUserRent.current_money -= dh.total_amount;
                    //increase amount user dou
                    getUserDou.current_money += (dh.total_amount) - ((dh.total_amount * 10) / 100);

                    db.SaveChanges();
                    return Json(1);//success
                }
            } catch (Exception ex)
            {
                return Json(-1);
            }
        }
        //reject request
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Reject(int userIdDou, int userIdRent, DateTime createdAt)
        {
            try
            {
                rent_transaction checkTimeRent = db.rent_transaction.OrderByDescending(x => x.created_at)
                    .Where(x => x.user_id_dou == userIdDou && x.user_id_rent == userIdRent).FirstOrDefault();
                if (checkTimeRent != null)
                {
                    DateTime now = DateTime.Now;
                    DateTime getCreatedAt = checkTimeRent.created_at;
                    TimeSpan getDistance = now - getCreatedAt;
                    if (getDistance.Days >= 1 || getDistance.Hours >= 1 || getDistance.Minutes > 20)
                        return Json(0);//timeout
                }
                rent_transaction checkRent = db.rent_transaction.Where(x => x.user_id_dou.Equals(userIdDou))
                    .Where(x => x.user_id_rent.Equals(userIdRent))
                    .Where(x => DbFunctions.TruncateTime(x.created_at) == DbFunctions.TruncateTime(createdAt)).SingleOrDefault();
                if(checkRent == null)
                    return Json(-1);//error
                else
                {
                    checkRent.status = 2;//change status to reject
                    db.SaveChanges();
                    return Json(1);//success
                }
            }
            catch (Exception ex)
            {
                return Json(-1);
            }
        }
        public ActionResult NotificationHeader()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            //get request transaction
            var getTransactionDou = db.rent_transaction.OrderByDescending(x => x.created_at)
                .Where(x => x.user_id_dou == session.User_id && x.status == 1).ToList();
            //get successful transaction
            var getTransactionRent = db.rent_transaction.OrderByDescending(x => x.created_at)
                .Where(x => x.user_id_rent == session.User_id).Take(7).ToList();
            ViewBag.getTransactionDou = (getTransactionDou == null) ? null : getTransactionDou;
            ViewBag.getTransactionRent = (getTransactionRent == null) ? null : getTransactionRent;
            return PartialView();
        }

        [HttpGet]
        public ActionResult ForgetPass()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ForgetPass(users user)
        {
            var tam = new AccountModel();

            if (tam.Check_Email(user.email))
            {   // lấy các thuộc tính cần truyền cho sendmail.htnl
                var userforget = new users();

                // lấy dối tượng user có email dăng kí
                userforget = tam.GetBymail(user.email);

                // lấy thuộc tính name user cho sendmail
                var name = userforget.user_name;

                //tạo và gán pass mới
                Random rd = new Random();
                var pass = Convert.ToString(rd.Next(100000, 999999));



                //kiểm tra kết quả update pass

                if (tam.updatpass(pass, userforget.id))//truyền id và pass mới
                {//gửi mail

                    string content = System.IO.File.ReadAllText(Server.MapPath("~/Views/Templet/sendmail.html"));
                    content = content.Replace("{{displayname}}", name);
                    content = content.Replace("{{password}}", pass);
                    var toEmail = user.email;
                    new MailHelpers().SendMail(toEmail, "Mật khẩu của khách hàng", content);
                    ViewBag.succes = "Mật khẩu đã được gửi về email đăng kí. Kiểm tra Spam nếu không thấy trong hộp thư";




                }
                else
                {
                    ViewBag.fail = "Xẩy ra lỗi - Vui lòng thử lại!";
                }



            }
            else
            {
                ModelState.AddModelError("", "Email chưa được đăng ký ");
            }


            //Response.Redirect(Request.Url.ToString(), true);
            return View();

        }

        [HttpGet]
        public ActionResult Update_pass()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            ViewBag.success = null;
            if (session != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update_pass(update_pass user)
        {
            var member = new AccountModel();
            if (ModelState.IsValid)
            {
                var session = (UserSession)Session[CommonConstants.USER_SESSION];
                var name = session.UserName;
                var oldpass = user.old_password;
                var pass = user.new_password;

                if (member.check_pass(name, oldpass))
                {
                    int id = member.GetById(name).id;
                    if (member.updatpass(pass,id))
                    {
                        ViewBag.success = "aa";
                    }
                    else
                    {
                        ViewBag.fail = "Xảy ra lỗi- Vui lòng thử lại";
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Mật khẩu không đúng");
                }

            }
            else
            {
                ViewBag.mess = "Dữ liệu nhập vào không hợp lệ - Vui lòng thử lại";
            }
            return View();

        }
        [HttpGet]
        public ActionResult Infor_user()
        {
            var sesion = (UserSession)Session[CommonConstants.USER_SESSION];
            if (sesion != null)
            {
                var tam = new AccountModel();
                var model = new user_sp();
                var user = new users();
                string name = sesion.UserName;
                user = tam.GetById(name);
                model.display_name = user.display_name;
                model.gender = user.gender;
                model.phone = user.phone;
                model.avatar = user.avatar;
                model.city_id = user.city_id;
                SetViewbag();
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Infor_user(user_sp update_infor)
        {
            var sesion = (UserSession)Session[CommonConstants.USER_SESSION];
            var name = sesion.UserName;
            int id = res.GetById(name).id;

            if (ModelState.IsValid)
            {
                //cập nhật
                if (res.Update_infor(update_infor, id))
                {
                    ViewBag.succes = "Cập nhật thành công";
                }
                else
                {
                ViewBag.fail = "Xảy ra lỗi , vui lòng kiểm tra và thử lại";
                }
            }
            else
            {
                ViewBag.mess = "Dữ liệu nhập không hợp lệ";
            }
            update_infor.avatar = res.GetById(name).avatar;
            SetViewbag();
            return View(update_infor);
        }
        public void postfile(HttpPostedFileBase file)
        {
            if (file != null) //This is a secret technique that takes  2 days of Minh :v :v :v
            { // Xóa file ảnh hiện tại    
                //string nameuser = ((UserSession)Session[CommonConstants.USER_SESSION]).user_name;
                //var tam = new AccountModel();
                //string avatar = tam.GetById(nameuser).avatar;
                //string directoryimg = Server.MapPath("~/Content/assets/img/" + avatar);
                //System.IO.File.Delete(directoryimg);

                //Up file ảnh mới.
                file.SaveAs(Server.MapPath("~/Content/assets/img/" + file.FileName));
            }
           
        }
        public void SetViewbag()
        {
            //DropList City
            ViewBag.CityID = new SelectList(res.listciy(), "id", "name");

            // Droplist Gender
            List<SelectListItem> sex_items = new List<SelectListItem>();
            sex_items.Add(new SelectListItem() { Text = "Nam", Value = "1" });
            sex_items.Add(new SelectListItem() { Text = "Nữ", Value = "2" });
            ViewBag.Gender = sex_items;

        }
        public ActionResult PlayerInfo()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if(session != null) { 
            var games = db.games.ToList();
            ViewBag.games = games;
            ViewBag.success = null;
            UserSupport us = new UserSupport();
            var models = us.ViewDetail(session.User_id);
            if (models == null)
            {
                models = new InfoPlayerViewModel();
            } else
            {
                models.game_id = us.getPlayerGame(session.User_id);
            }
            return View(models);
            }
            else
            {
                return RedirectToAction("Login", "Login");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PlayerInfo(InfoPlayerViewModel ip)
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var games = db.games.ToList();
                ViewBag.games = games;
                decimal getAmount = Convert.ToDecimal(ip.amount_in_one);
                var sss = getAmount % 5000;
                if (ModelState.IsValid) {
                    if (getAmount < 10000 || getAmount > 10000000)
                    {
                        ModelState.AddModelError("amount_in_one", "Số tiền thuê từ 10,000 đến 1,000,000 VND");
                    }
                    else if ((getAmount % 5000) != 0)
                    {
                        ModelState.AddModelError("amount_in_one", "Số tiền thuê phải là bội số của 5000");
                    }
                    //inserting game_id and user_id into pg table
                    var getRemove = db.player_game.Where(x => x.user_id.Equals(session.User_id));
                    if (getRemove != null && getRemove.Count() > 0)
                    {
                        db.player_game.RemoveRange(db.player_game.Where(c => c.user_id == session.User_id));
                        db.SaveChanges();
                    }
                    foreach (var idGame in ip.game_id)
                    {
                        player_game plNew = new player_game();
                        plNew.user_id = session.User_id;
                        plNew.game_id = idGame;
                        db.player_game.Add(plNew);
                        db.SaveChanges();
                    }
                    info_players ipCheck = db.info_players.SingleOrDefault(x => x.user_id == session.User_id);
                    //change status user
                    if (ipCheck == null)
                    {
                        users user = db.users.SingleOrDefault(x => x.id.Equals(session.User_id));
                        user.status = 1;
                        //add new info
                        info_players ipNew = new info_players();
                        ipNew.user_id = session.User_id;
                        ipNew.title = ip.title;
                        ipNew.description = ip.description;
                        ipNew.link_fb = ip.link_fb;
                        ipNew.link_insta = ip.link_insta;
                        ipNew.link_messenger = ip.link_messenger;
                        ipNew.link_youtube = ip.link_youtube;
                        ipNew.amount_in_one = ip.amount_in_one;
                        ipNew.tag = ip.tag;
                        db.info_players.Add(ipNew);
                        db.SaveChanges();
                    }
                    else
                    {
                        ipCheck.title = ip.title;
                        ipCheck.description = ip.description;
                        ipCheck.link_fb = ip.link_fb;
                        ipCheck.link_insta = ip.link_insta;
                        ipCheck.link_messenger = ip.link_messenger;
                        ipCheck.link_youtube = ip.link_youtube;
                        ipCheck.amount_in_one = ip.amount_in_one;
                        ipCheck.tag = ip.tag;
                        db.SaveChanges();
                    }
                    ViewBag.success = "Cập nhật thành công";
                }
                return View(ip);
            }
            return RedirectToAction("Login", "Login");
        }
        public ActionResult PlayerStatistic()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                return View();
            }
            return RedirectToAction("Login", "Login");
        }
        public ActionResult GeneInfo()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var ifUser = db.info_players.SingleOrDefault(x => x.user_id == session.User_id);
                if (ifUser == null)
                {
                    ViewBag.ifUser = null;
                } else
                {
                    ViewBag.ifUser = ifUser;
                    var sevenDayAgo = DateTime.Now.AddDays(-6);
                    GeneInfoUser getTotalAmount = new GeneInfoUser();
                    var getSevenDay = (from a in db.dou_histories
                                       where a.created_at >= sevenDayAgo && a.user_id_dou == session.User_id
                                       group a by a.user_id_dou into g
                                       select new GeneInfoUser()
                                       {
                                           totalAmount = g.Sum(x => x.total_amount)
                                       }).SingleOrDefault();
                    getTotalAmount = getSevenDay;
                    ViewBag.totalSevenDay = (getTotalAmount == null) ? null : getTotalAmount;
                }
                return PartialView();
            }
            return RedirectToAction("Login", "Login");
        }
        public ActionResult History()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            if (session != null)
            {
                var model = db.info_players.SingleOrDefault(x => x.user_id.Equals(session.User_id));
                if (model == null)
                    return View("~/Views/NotFound-404.cshtml");
                return View(model);
            }
            return RedirectToAction("Login", "Login");
        }
        [HttpPost]
        public JsonResult GetStatistic()
        {
            var session = (UserSession)Session[CommonConstants.USER_SESSION];
            List<DateTime> arrSevenday = new List<DateTime>();
            List<GeneInfoUser> arrData = new List<GeneInfoUser>();
            List<string> arrSevendayConvert = new List<string>();
            List<string> arrDataConvert = new List<string>();
            //get last seven day
            for (int i = 0; i <= 6; i++)
            {
                DateTime nDaysAgo;
                if (i == 0)
                {
                    nDaysAgo = DateTime.Today.AddDays(i);
                } else
                {
                    nDaysAgo = DateTime.Today.AddDays(-i);
                }
                arrSevenday.Add(nDaysAgo);
            }
            //get data last seven day
            for (int i = 0; i <= 6; i++)
            {
                var convertDate = arrSevenday[i];
                var getData = db.dou_histories.
                Where(x => x.user_id_dou == session.User_id).
                Where(x => DbFunctions.TruncateTime(x.created_at) == DbFunctions.TruncateTime(convertDate)).
                GroupBy(x => x.user_id_dou).
                Select(x => new GeneInfoUser { totalAmount = x.Sum(a => a.total_amount) }).
                OrderBy(x => x.totalAmount).FirstOrDefault();
                if (getData == null)
                {
                    GeneInfoUser newIf = new GeneInfoUser();
                    newIf.totalAmount = 0;
                    arrData.Add(newIf);
                }
                else
                {
                    arrData.Add(getData);
                }
            }
            foreach (var get in arrSevenday)
            {
                arrSevendayConvert.Add(get.ToShortDateString());
            }
            foreach (var get in arrData)
            {
                arrDataConvert.Add(get.totalAmount.ToString());
            }
            var merge = arrSevendayConvert.Concat(arrDataConvert);
            return Json(merge);
        }
    }
}