﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cdio447.Supports
{
    public class RandomDigits
    {
        public string StartRandom()
        {
            int length = 9;
            var random = new Random();
            string s = string.Empty + "0";
            for (int i = 0; i < length; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }
    }
}