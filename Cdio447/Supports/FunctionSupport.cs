﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cdio447.Supports
{
    public static class FunctionSupport
    {
        public static string convertVND(decimal value, string suffix)
        {
            string str = value.ToString();
            str = str.Replace(",00","");
            var len = str.Length;
            var counter = 3;
            var result = "";
            while (len - counter >= 0) {
            var con = str.Substring(len - counter, 3);
                result = "," + con + result;
                counter += 3;
            }
            var con2 = str.Substring(0, 3 - (counter - len));
            result = con2 + result;
            if (result.Substring(0, 1) == ",")
            {
                result = result.Substring(1, result.Length - 1);
            }
            result = result + suffix;
            return result;
        }
        public static string convertDatetimeVN(DateTime dt)
        {
            return dt.ToString("dd/MM/yyyy - HH:mm:ss");
        }
        public static string RandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            var finalString = new String(stringChars);
            return finalString;
        }
    }
}